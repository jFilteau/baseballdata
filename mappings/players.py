PLAYERS_MAPPING = {
    "id": ["id"],
    "fullName": ["fullName"],
    "firstName": ["firstName"],
    "middleName": ["middleName"],
    "lastName": ["lastName"],
    "position": ["primaryPosition", "abbreviation"],
    "teamId": ["teamId"],
    "strikeZoneTop": ["strikeZoneTop"],
    "strikeZoneBottom": ["strikeZoneBottom"],
    "batSide": ["batSide", "description"],
    "pitchHand": ["pitchHand", "description"]
}
