TEAM_MAPPING = {
    "id": ["id"],
    "fullName": ["name"],
    "teamName": ["teamName"],
    "locationName": ["locationName"],
    "abbreviation": ["abbreviation"],
    "league": ["league", "id"],
    "division": ["division", "id"],
    "venueName": ["venue", "name"]
}
