GAMES_MAPPING = {
    "gamePk": ["gameData", "game", "pk"],
    "date": ["gameData", "datetime", "originalDate"],
    "awayTeam": ["gameData", "teams", "away", "id"],
    "homeTeam": ["gameData", "teams", "home", "id"],
    "weatherCondition": ["gameData", "weather", "condition"],
    "temperature": ["gameData", "weather", "temp"],
    "wind": ["gameData", "weather", "wind"],
    "dayOrNight": ["gameData", "datetime", "dayNight"],
    "officialHomeplate": ["liveData", "boxscore", "officials"]
}
