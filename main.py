import sys
from datetime import datetime
import argparse
from apiHandlers.initializer import Initializer
from database.databaseHandler import DatabaseHandler
from shared.dataValidators import DataValidators
from shared.dataTransformers import DataTransformers
from scripts.dataHandlers.teams import TeamDataHandler
from scripts.dataHandlers.players import PlayerDataHandler
from scripts.dataHandlers.officials import OfficialDataHandler
from scripts.dataHandlers.playEvents import PlayEventsDataHandler
from scripts.dataHandlers.gamesInfo import GamesInfoDataHandler
from scripts.dataHandlers.matchups import MatchupsDataHandler
from scripts.dataHandlers.pitches import PitchesDataHandler
from scripts.dataHandlers.boxscores import BoxscoresDataHandler
from scripts.scrapers.baseballSavant import BaseballSavantScraper

class Main:
    def __init__(self):
        self.database_handler = DatabaseHandler()

    def init(self, start_date, end_date):
        if not self.database_handler:
            return False

        api_initializer = Initializer()

        team_data = api_initializer.fetch_team_data()
        teams_to_insert = TeamDataHandler().init(team_data)
        if DataValidators.list_is_valid(teams_to_insert):
            self.database_handler.bulk_insert_in_db(teams_to_insert)

        live_data = api_initializer.fetch_live_data(start_date, end_date)
        if DataValidators.list_is_valid(live_data):
            players_to_insert = PlayerDataHandler().init(live_data)
            if DataValidators.list_is_valid(players_to_insert):
                self.database_handler.bulk_insert_in_db(players_to_insert)

            officials_to_insert = OfficialDataHandler().init(live_data)
            if DataValidators.list_is_valid(officials_to_insert):
                self.database_handler.bulk_insert_in_db(officials_to_insert)

            play_events_to_insert = PlayEventsDataHandler().init(live_data)
            if DataValidators.list_is_valid(play_events_to_insert):
                self.database_handler.bulk_insert_in_db(play_events_to_insert)

            games_to_insert = GamesInfoDataHandler().init(live_data)
            if DataValidators.list_is_valid(games_to_insert):
                self.database_handler.bulk_insert_in_db(games_to_insert)

            matchups_to_insert = MatchupsDataHandler().init(live_data)
            if DataValidators.list_is_valid(matchups_to_insert):
                self.database_handler.bulk_insert_in_db(matchups_to_insert)

            pitches_to_insert = PitchesDataHandler().init(live_data)
            if DataValidators.list_is_valid(pitches_to_insert):
                self.database_handler.bulk_insert_in_db(pitches_to_insert)

            boxscores_to_insert = BoxscoresDataHandler().init(live_data)
            if DataValidators.list_is_valid(boxscores_to_insert):
                self.database_handler.bulk_insert_in_db(boxscores_to_insert)

            self.database_handler.close_session()

def date_validator(date):
    try:
        return datetime.strptime(date, "%m/%d/%Y")
    except:
        raise argparse.ArgumentTypeError("Date format should be MM/DD/YYYY")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Baseball data engine.")
    parser.add_argument("--i", nargs=2,
            metavar=("START-DATE", "END-DATE"),
            type=date_validator,
            help="import data")
    parser.add_argument("--u", nargs=2,
            metavar=("START-DATE", "END-DATE"),
            type=date_validator,
            help="update data")
    args = parser.parse_args()

    START_DATE, END_DATE = args.i if args.i is not None else args.u
    if START_DATE > END_DATE:
        raise ValueError("Please enter a start date lower than the end date.")

    if args.i is not None:
        # We use this string time format since it's the format used in the api.
        START_DATE = START_DATE.strftime("%m/%d/%Y")
        END_DATE   = END_DATE.strftime("%m/%d/%Y")
        Main().init(START_DATE, END_DATE)  
    else:
        # We use this string time format since it's the format used in MySQL.
        START_DATE = START_DATE.strftime("%Y/%m/%d")
        END_DATE   = END_DATE.strftime("%Y/%m/%d")
        BaseballSavantScraper().main(START_DATE, END_DATE)
