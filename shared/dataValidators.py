class DataValidators:
    @staticmethod
    def object_is_valid(obj):
        return isinstance(obj, dict) and len(obj.keys()) != 0

    @staticmethod
    def list_is_valid(_list):
        return isinstance(_list, list) and len(_list) != 0

    @staticmethod
    def string_is_valid(_str):
        _str = _str.strip()
        return isinstance(_str, str) and len(_str) != 0
