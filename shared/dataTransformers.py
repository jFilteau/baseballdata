from shared.dataValidators import DataValidators
import json


class DataTransformers:
    @staticmethod
    def convert_to_json_object(obj_to_convert):
        if not DataValidators.string_is_valid(obj_to_convert):
            return False

        try:
            converted_value = json.loads(obj_to_convert)
            if not DataValidators.object_is_valid(converted_value):
                return False
            return converted_value  # Only return if the loaded json passes the test above
        except json.decoder.JSONDecodeError:
            return False

    @staticmethod
    def convert_to_string_object(obj_to_convert):
        if not DataValidators.object_is_valid(obj_to_convert):
            return False
        converted_value = json.dumps(obj_to_convert, sort_keys=True)
        return False if not DataValidators.string_is_valid(converted_value) else converted_value

    @staticmethod
    def stringify_list_of_objects(list_of_objects):
        """Returns a list of objects converted to strings.

        list_of_objects: array - array of objects
        """
        stringified_list = []
        for obj in list_of_objects:
            if DataValidators.object_is_valid(obj):
                converted_obj = DataTransformers.convert_to_string_object(obj)
                stringified_list.append(converted_obj)
        return False if not DataValidators.list_is_valid(stringified_list) else stringified_list
