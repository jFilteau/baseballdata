from shared.dataValidators import DataValidators
from shared.dataTransformers import DataTransformers


class DataHandler:
    def get_days_list_from_response_data(self, response_data):
        """Returns the list of games data split by day.

        response_data: dict - data returned from the schedule API
        """
        try:
            return response_data["dates"]
        except (AttributeError, ValueError, KeyError) as e:
            return False

    def game_is_valid(self, game_data):
        """Returns True if the game was in the regular season and finalized,
        False otherwise.

        game_data: dict - Data for a single game.
        """
        try:
            game_type   = game_data["gameType"].strip().lower()
            status_code = game_data["status"]["codedGameState"].strip().lower()
            return status_code in ["f", "o", "q", "r"] and game_type == "r"
        except (AttributeError, ValueError, KeyError) as e:
            return False

    def get_game_pks_from_schedule_response_data(self, response_data):
        """Returns a list of the unique game keys of all games played in the timespan.

        response_data: dict - of data returned from the schedule API
        """
        if not response_data:
            return False

        days_list = self.get_days_list_from_response_data(response_data)
        if not DataValidators.list_is_valid(days_list):
            return False

        games_list = self.create_list_of_games_from_multiple_days(days_list)
        if not DataValidators.list_is_valid(games_list):
            return False

        all_games_pks = []
        for game_data in games_list:
            if self.game_is_valid(game_data):
                try:
                    all_games_pks = all_games_pks + [game_data["gamePk"]]
                except (ValueError, KeyError) as e:
                    continue

        if not DataValidators.list_is_valid(all_games_pks):
            return False

        return all_games_pks

    def create_list_of_games_from_multiple_days(self, days_list):
        """Returns a single list of all games played in a timespan of days.

        days_list: dict - data for a span of days
        """
        if not DataValidators.list_is_valid(days_list):
            return False

        games_list = []
        for single_day_data in days_list:
            try:
                games_list.extend(single_day_data["games"])
            except (ValueError, KeyError) as e:
                continue

        if not DataValidators.list_is_valid(games_list):
            return False

        return games_list

    def get_live_data_of_game(self, game_data):
        """Returns the live data dict for a given game.

        game_data: dict - data for a given game
        """
        try:
            return game_data["liveData"]
        except (ValueError, KeyError) as e:
            return False

    def get_plays_data_of_game(self, game_data):
        """Returns the list of plays for a given game.

        game_data: dict - data for a given game
        """
        try:
            return game_data["plays"]["allPlays"]
        except (ValueError, KeyError) as e:
            return False
