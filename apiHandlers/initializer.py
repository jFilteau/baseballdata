import requests
from multiprocessing import Pool
from apiHandlers.responseDataHandler import DataHandler
from shared.dataValidators import DataValidators
from shared.dataTransformers import DataTransformers


class Initializer:
    def __init__(self):
        self.data_handler = DataHandler()

    def create_schedule_url(self, start_date, end_date):
        """Return the API url string that gives us schedule information."""
        if not start_date or not end_date:
            return False
        return ("http://statsapi.mlb.com/api/v1/schedule?"
                "sportId=1&startDate={start}&endDate={end}").format(
                        start=start_date,
                        end=end_date)

    def fetch_schedule_data(self, start_date, end_date):
        """Returns the data received from the schedule API url string."""
        schedule_url = self.create_schedule_url(start_date, end_date)
        if not schedule_url:
            return False

        response = requests.get(schedule_url)
        if not response.ok:
            return False

        return response.json()

    def get_api_data_of_all_teams(self):
        """Returns the data of all teams in the league."""
        url = "http://statsapi.mlb.com/api/v1/teams?sportId=1"
        response = requests.get(url)
        if not response.ok:
            return False
        return response.json()

    def fetch_team_data(self):
        """Returns a list of dicts containing the required data for each team."""
        try:
            api_team_data = self.get_api_data_of_all_teams()
            return api_team_data["teams"]
        except (AttributeError, ValueError, KeyError) as e:
            return False

    def get_game_feed_live_data(self, game_pk):
        """Returns the live data for a single game.

        game_pk: integer - unique game key
        """
        if not game_pk:
            return False
        url = "https://statsapi.mlb.com/api/v1.1/game/{}/feed/live".format(game_pk)
        response = requests.get(url)
        if not response.ok:
            return False
        return {"response": response.json(), "gamePk": game_pk}

    def fetch_game_feeds(self, game_pks):
        with Pool() as p:
            game_feed_api_responses = p.map(self.get_game_feed_live_data, game_pks)

        if not DataValidators.list_is_valid(game_feed_api_responses):
            return False

        valid_responses = [r for r in game_feed_api_responses if r]
        if not DataValidators.list_is_valid(valid_responses):
            return False

        return valid_responses

    def fetch_live_data(self, start_date, end_date):
        schedule_data_from_api = self.fetch_schedule_data(start_date, end_date)
        if not schedule_data_from_api:
            return False

        game_pks = self.data_handler.get_game_pks_from_schedule_response_data(schedule_data_from_api)
        if not game_pks:
            return False
        
        game_feed_responses = self.fetch_game_feeds(game_pks)
        if not game_feed_responses:
            return False

        try:
            return [r["response"] for r in game_feed_responses]
        except (ValueError, KeyError) as e:
            return False
