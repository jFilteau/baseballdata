import csv
import logging
import pandas as pd
from multiprocessing import Pool, cpu_count
from database.databaseHandler import DatabaseHandler


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("baseballsavant-scraper")
def get_updates(matchup_id_to_url):
    if not isinstance(matchup_id_to_url, tuple) or len(matchup_id_to_url) != 2:
        return False

    try:
        matchup_id, url = matchup_id_to_url
        data           = pd.read_csv(url)
        exit_velo      = data[data["launch_speed"].notnull()]["launch_speed"].values
        launch_angle   = data[data["launch_angle"].notnull()]["launch_angle"].values
        updates = {
                "id": matchup_id,
                "hitLaunchSpeed": float(exit_velo[0]),
                "hitLaunchAngle": float(launch_angle[0])
        }
    except Exception as e:
        return False

    return updates


class BaseballSavantScraper:
    def __init__(self):
        self.db_handler = DatabaseHandler()

    def get_missing_exit_velocities(self, start_date, end_date):
        """Returns a list of all the rows that are missing an exit velocity
        despite the ball being put in play.

        start_date : string - String of date in the format YYYY/MM/DD
        end_date   : string - String of date in the format YYYY/MM/DD
        """
        if not start_date or not end_date:
            return False

        results = self.db_handler.get_missing_launch_speeds(start_date, end_date)
        if not results or not isinstance(results, list) or len(results) == 0:
            return False

        return results

    def get_matchup_info(self, row_proxy):
        """Returns a tuple containing the matchup information.

        row_proxy: proxy - Data representing a row in the database.
        """
        try:
            matchup_info = (
                    row_proxy["batterId"],
                    row_proxy["inning"],
                    row_proxy["matchupId"]
            )
        except Exception as e:
            raise e
        return(matchup_info)

    def get_every_matchup_info_by_pitcher(self, dataset):
        """Returns the information of each at bat separated by pitcher.

        dataset: array - List of row proxies from the database.
        """
        if not dataset:
            return False

        pitcher_id_to_matchups = {}
        for row_proxy in dataset:
            try:
                matchup_info = self.get_matchup_info(row_proxy)
                pitcher_id   = row_proxy["pitcherId"]
            except Exception as e:
                raise e

            if pitcher_id not in pitcher_id_to_matchups.keys():
                # Create a set to make sure there are no duplicates.
                pitcher_id_to_matchups[pitcher_id] = set()

            pitcher_id_to_matchups[pitcher_id].add(matchup_info)

        if len(pitcher_id_to_matchups.keys()) == 0:
            return False

        return pitcher_id_to_matchups

    def create_request_url(self, pitcher_id, matchup_info, start_date, end_date):
        """Returns the url string to fetch data from.

        pitcher_id   : int    - Pitcher unique id. 
        matchup_info : tuple  - Tuple of the matchup info.
        start_date   : string - String of date in the format YYYY/MM/DD
        end_date     : string - String of date in the format YYYY/MM/DD
        """
        start_date = start_date.replace("/", "-")
        end_date   = end_date.replace("/", "-")
        batter_id  = matchup_info[0]
        inning     = matchup_info[1]

        url = ("https://baseballsavant.mlb.com/statcast_search/csv?all=true&"
                "hfPT=&hfAB=&hfBBT=&hfPR=&hfZ=&stadium=&hfBBL=&hfNewZones=&"
                "hfGT=R%7C&hfC=&hfSea=2019%7C&hfSit=&player_type=pitcher&"
                "hfOuts=&opponent=&pitcher_throws=&batter_stands=&hfSA=&"
                "game_date_gt={start}&game_date_lt={end}&hfInfield=&"
                "team=&position=&hfOutfield=&hfRO=&home_road=&"
                "batters_lookup%5B%5D={batter}&hfFlag=&hfPull=&"
                "pitchers_lookup%5B%5D={pitcher}&metric_1=&hfInn={inning}%7C&"
                "min_pitches=0&min_results=0&group_by=name&sort_col=pitches&"
                "player_event_sort=h_launch_speed&sort_order=desc&min_pas=0&").format(
                       start=start_date,
                       end=end_date,
                       pitcher=pitcher_id,
                       inning=inning,
                       batter=batter_id)

        return url

    def get_matchup_id_urls_to_fetch(self, pitcher_id_to_matchups, start_date, end_date):
        """Returns the url to fetch for every matchup that's missing an exit velocity.

        pitcher_id_to_matchups : dict   - Matchup info for each at bat by pitcher
        start_date             : string - String of date in the format YYYY/MM/DD
        end_date               : string - String of date in the format YYYY/MM/DD
        """
        if not pitcher_id_to_matchups or not start_date or not end_date:
            return False

        play_id_to_url = set()
        for pitcher_id, matchups in pitcher_id_to_matchups.items():
            for matchup_info in matchups:
                url = self.create_request_url(pitcher_id, matchup_info, start_date, end_date)
                if not url:
                    continue

                try:
                    matchup_id = matchup_info[2]
                    play_id_to_url.add((matchup_id, url))
                except Exception as e:
                    raise e

        if len(play_id_to_url) == 0:
            return False

        return list(play_id_to_url)

    def get_column_update_values(self, matchup_id_to_url):
        """Returns the columns to update with the new value for each matchup
        that's missing data.

        play_id_to_url : dict - Urls to fetch for each unique play id.
        """
        if not isinstance(matchup_id_to_url, list):
            return False

        with Pool(cpu_count() - 1) as pool:
            update_values = pool.map(get_updates, matchup_id_to_url)

        update_values = list(filter(None, update_values))
        if len(update_values) == 0:
            return False

        return update_values

    def main(self, start_date, end_date):
        missing_exit_velos = self.get_missing_exit_velocities(start_date, end_date)
        if not missing_exit_velos:
            logger.info("There are no missing exit velocities.")
            return False

        pitcher_id_to_matchups = self.get_every_matchup_info_by_pitcher(missing_exit_velos)
        if not pitcher_id_to_matchups:
            return False

        matchup_id_to_url = self.get_matchup_id_urls_to_fetch(pitcher_id_to_matchups, start_date, end_date)
        if not matchup_id_to_url:
            logger.info("There is no baseballsavant data to fetch.")
            return False

        column_updates = self.get_column_update_values(matchup_id_to_url)
        if not column_updates:
            logger.info("Baseballsavant does not contain the missing values required.")
            return False

        filepath = "scripts/scrapers/logs/updated-matchups_{start}_to_{end}.csv".format(
                start=start_date.replace("/", "-"),
                end=end_date.replace("/", "-")
        )
        with open(filepath, "w") as csv_file:
            writer = csv.writer(csv_file)

            # It's easier to have the primary key id as the first column.
            header = ["id"] + [col for col in list(column_updates[0]) if col != "id"]
            writer.writerow(header)

            logger.info("Starting database update")
            self.db_handler.bulk_update_matchups(column_updates)
            logger.info("Database update succesful.")
            for update in column_updates:
                row = [update[header[0]], update[header[1]], update[header[2]]]
                writer.writerow(row)
