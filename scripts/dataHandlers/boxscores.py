from scripts.dataHandlers.base import BaseHandler
from mappings.boxscores import BOXSCORES_MAPPINGS


class BoxscoresDataHandler(BaseHandler):
    def __init__(self):
        super(BoxscoresDataHandler, self).__init__()
        self.boxscores_table_unique_columns = self.database_handler.get_table_unique_constraint_column_names("boxscores")

    def create_boxscore(self, boxscore_info):
        if not super().validate_object(boxscore_info):
            return False
        return self.database_handler.create_boxscore(boxscore_info)

    def convert_boxscores_to_table_objects(self, boxscores_list):
        """Returns a list of each boxscore object mapping converted to a table object.

        boxscores_list: list - List of dictionaries with each one representing the boxscore mapping values of a game.
        """
        if not super().validate_list(boxscores_list):
            return False
        
        converted_boxscores_list = []
        for boxscore_data in boxscores_list:
            boxscore_object = self.create_boxscore(boxscore_data)
            if boxscore_object:
                converted_boxscores_list.append(boxscore_object)

        return False if not super().validate_list(converted_boxscores_list) else converted_boxscores_list

    def update_boxscore_data_mapping(self, boxscore_data):
        """Returns the updated boxscore data.

        boxscore_data: dict - data to update
        """
        if not super().validate_object(boxscore_data):
            return False
        if "homeTeamPitchingInningsPitched" not in boxscore_data.keys() or "awayTeamPitchingInningsPitched" not in boxscore_data.keys():
            return False

        try:
            boxscore_data["homeTeamPitchingInningsPitched"] = float(boxscore_data["homeTeamPitchingInningsPitched"])
            boxscore_data["awayTeamPitchingInningsPitched"] = float(boxscore_data["awayTeamPitchingInningsPitched"])
            return boxscore_data
        except:
            return False

    def get_boxscores_from_database(self):
        """Returns a list of tuples with each tuple representing the unique contraint values of each record in the boxscores table."""
        if not self.boxscores_table_unique_columns:
            return False

        boxscores_table_object = self.database_handler.get_table_object("boxscores")
        if not boxscores_table_object:
            return False

        unique_contraint_tuples = self.database_handler.get_table_column_values(boxscores_table_object, self.boxscores_table_unique_columns)
        return False if not unique_contraint_tuples else unique_contraint_tuples

    def get_boxscore_mapping_values_for_each_game(self, data_by_game):
        """Returns a list of each boxscore object mapping.

        data_by_game: list - List of dictionaries with each one representing the API data of a game.
        """
        boxscore_data_by_game = super().get_mapping_values(BOXSCORES_MAPPINGS, data_by_game)
        return False if not boxscore_data_by_game else boxscore_data_by_game

    def update_boxscore_mapping_values(self, boxscore_mapping_values):
        """Returns a list of each boxscore object mapping with updated values.

        boxscore_mapping_values: list - List of dictionaries with each one representing the mapping value of a game.
        """
        if not super().validate_list(boxscore_mapping_values):
            return False

        updated_boxscore_mapping_list = []
        for boxscore_mapping in boxscore_mapping_values:
            updated_boxscore_mapping = self.update_boxscore_data_mapping(boxscore_mapping)
            if super().validate_object(updated_boxscore_mapping):
                updated_boxscore_mapping_list.append(updated_boxscore_mapping)

        return False if not super().validate_list(updated_boxscore_mapping_list) else updated_boxscore_mapping_list
        
    def init(self, data):
        boxscores_data = self.get_boxscore_mapping_values_for_each_game(data)
        if not boxscores_data:
            return False

        updated_boxscores_data = self.update_boxscore_mapping_values(boxscores_data)
        if not updated_boxscores_data:
            return False

        boxscore_values_converted_to_table_objects = self.convert_boxscores_to_table_objects(updated_boxscores_data)
        if not boxscore_values_converted_to_table_objects:
            return False

        boxscores_already_in_db = self.get_boxscores_from_database()
        boxscores_to_insert = self.remove_objects_already_in_table(boxscore_values_converted_to_table_objects, boxscores_already_in_db,
                self.boxscores_table_unique_columns)

        return False if not super().validate_list(boxscores_to_insert) else boxscores_to_insert
