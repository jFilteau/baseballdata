from functools import reduce
from shared.dataValidators import DataValidators
from database.databaseHandler import DatabaseHandler


class BaseHandler:
    def __init__(self):
        self.database_handler = DatabaseHandler()

    def validate_list(self, list_to_validate):
        return DataValidators.list_is_valid(list_to_validate)

    def validate_object(self, object_to_validate):
        return DataValidators.object_is_valid(object_to_validate)

    def get_live_data(self, game_data):
        """Returns the live data dict for a given game, i.e. the liveData key.

        game_data: dict - data for a given game
        """
        if not DataValidators.object_is_valid(game_data) or "liveData" not in game_data.keys():
            return False
        live_data = game_data["liveData"]
        return False if not DataValidators.object_is_valid(live_data) else live_data
    
    def append_game_pk_to_at_bat_data(self, at_bats, game_pk):
        """Returns the a list of at bat data with each one one updated to contain the game PK.

        at_bats: list - all at bats for a given game
        game_pk: int - game PK for a given game
        """
        if not self.validate_list(at_bats) or not game_pk:
            return False

        at_bats_with_game_pk = []
        for at_bat_data in at_bats:
            if not self.validate_object(at_bat_data):
                continue
            at_bat_data.update({"gamePk": game_pk})
            at_bats_with_game_pk.append(at_bat_data)

        return False if not self.validate_list(at_bats_with_game_pk) else at_bats_with_game_pk

    def get_plays_data_of_game(self, game_data):
        """Returns the list of plays (pitches) for a given game.

        game_data: dict - data for a given game
        """
        if not self.validate_object(game_data) or "gamePk" not in game_data.keys():
            return False

        live_data = self.get_live_data(game_data)
        if not live_data or "plays" not in live_data.keys():
            return False

        plays = live_data["plays"]
        if not DataValidators.object_is_valid(plays) or "allPlays" not in plays.keys():
            return False

        all_plays = self.append_game_pk_to_at_bat_data(plays["allPlays"], game_data["gamePk"])
        return False if not DataValidators.list_is_valid(all_plays) else all_plays

    def get_key_value(self, dict_data, key):
        """Returns the dictionary's associated key value.

        dict_data: dict - data from which we want to retrieve the key's value
        key: string - key we want to retrieve from the dict
        """
        if not DataValidators.object_is_valid(dict_data):
            return False
        return dict_data.get(key, False)

    def get_data_value_by_trimming_obj_using_path(self, obj, path):
        """Returns the last value after filtering by the path.

        obj: dict - data from which we want to filter
        path: array - filter path that leads us to the required data

        This function filters the object based on the given path of keys.
        e.g. if the object is { key1: { key2: 1234 }}, then a path value of
        [key1, key2] would return 1234, [key1] returns {key2: 1234} and
        [invalidKey] returns False
        """
        if not DataValidators.object_is_valid(obj) or not DataValidators.list_is_valid(path):
            return False
        return reduce(self.get_key_value, path, obj)

    def get_db_column_mapping_values_from_object(self, mapping, data_object):
        """Returns a dict of the values extracted from the object based on the mapping.

        mapping: dict - db column name -> array of string keys leading to the required data in the data_object
        data_object: dict - data from which we want to extract the values
        """
        if not DataValidators.object_is_valid(mapping) or not DataValidators.object_is_valid(data_object):
            return False

        db_column_name_to_associated_data_object_value = {}
        for db_column_name, path_keys in mapping.items():
            data_object_value = self.get_data_value_by_trimming_obj_using_path(data_object, path_keys)
            db_column_name_to_associated_data_object_value[db_column_name] = data_object_value

        return db_column_name_to_associated_data_object_value

    def get_mapping_values(self, mapping, data):
        """Returns a list of dicts of the values extracted from the api data based on the mapping.

        mapping: dict - db column name -> array of string keys leading to the required data in the data_object
        api_data: array - data returned from the api
        mapping_updates: function - function that will update the mapping
        """
        if not DataValidators.object_is_valid(mapping) or not DataValidators.list_is_valid(data):
            return False

        all_mapping_values = []
        for d in data:
            mapping_values = self.get_db_column_mapping_values_from_object(mapping, d)
            if mapping_values:
                all_mapping_values.append(mapping_values)

        return False if not DataValidators.list_is_valid(all_mapping_values) else all_mapping_values

    def get_object_attribute_values(self, obj, attributes_to_get):
        """Returns a tuple of attributes from an object.

        obj: obj - Object from which we will extract the attribute values.
        attributes_to_get: list - List of attributes to extract from the object.
        """
        if not self.validate_list(attributes_to_get):
            return False
        try:
            return tuple([getattr(obj, attr) for attr in attributes_to_get])
        except AttributeError:
            return False

    def tuple_values_are_valid(self, _tuple):
        """Checks that no value in the tuple is False.

        _tuple: tuple - Tuple to validate.
        """
        if not _tuple or len(_tuple) == 0:
            return False
        for value in _tuple:
            if value is False:
                return False
        return True

    def remove_objects_already_in_table(self, objects_list_to_filter, db_objects_list, unique_columns_list):
        """Removes values in the objects list that are already in the db. To do so, we will extract the
        unique contraint (column) values of the object and compare it to the unique column values of the 
        db object list. If it exists then we remove said object; if not then we keep it.

        objects_list_to_filter: list - List of objects to filter, i.e. remove those already in the db
        db_objects_list: list - Objects list of what's already in the db.
        unique_columns_list: list - List of column names representing the unique constraint.
        """
        objects_not_in_database = {}

        for obj in objects_list_to_filter:
            unique_cotraint_attribute_values = self.get_object_attribute_values(obj, unique_columns_list)

            # There must be no False values for any unique contraint column.
            if self.tuple_values_are_valid(unique_cotraint_attribute_values):
                # There must either be no data in the database or the unique contraint values of the object
                # we want to insert must not already exist in the database objects list.
                if not db_objects_list or unique_cotraint_attribute_values not in db_objects_list:
                    # Updating the value associated to the key allows us to make sure there are no duplicates.
                    objects_not_in_database[unique_cotraint_attribute_values] = obj

        if len(objects_not_in_database.keys()) == 0:
            return False

        # We want to only return the object values without the unique contraint values tuple key.
        return [value for key, value in objects_not_in_database.items()]

    def get_all_play_by_game(self, data_by_game):
        """Convert the at bats of all the games into a single list.

        - data: list - List of objects with each one representing data for a single game.
        """
        if not self.validate_list(data_by_game):
            return False

        all_plays_by_game = []
        for single_game_data in data_by_game:
            all_plays = self.get_plays_data_of_game(single_game_data)
            if self.validate_list(all_plays):
                all_plays_by_game.append(all_plays)

        return False if len(all_plays_by_game) == 0 else all_plays_by_game
