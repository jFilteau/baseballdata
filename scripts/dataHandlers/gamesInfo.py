import re
from datetime import datetime
from scripts.dataHandlers.base import BaseHandler
from mappings.games import GAMES_MAPPING


class GamesInfoDataHandler(BaseHandler):
    def __init__(self):
        super(GamesInfoDataHandler, self).__init__()
        self.games_table_unique_columns = self.database_handler.get_table_unique_constraint_column_names("games")

    def create_game(self, game_info):
        if not super().validate_object(game_info):
            return False
        return self.database_handler.create_game(game_info)

    def convert_games_to_table_objects(self, games_list):
        """Returns a list of each game object mapping converted to a table object.

        games_list: list - List of dictionaries with each one representing the game mapping values of a game.
        """
        if not super().validate_list(games_list):
            return False
        
        converted_games_list = []
        for game_data in games_list:
            game_object = self.create_game(game_data)
            if game_object:
                converted_games_list.append(game_object)

        return False if not super().validate_list(converted_games_list) else converted_games_list

    def get_official_id(self, official_info):
        """Returns the id of the given official.

        official_info: object - Official's info (e.g. name, id, etc).
        """
        if not super().validate_object(official_info):
            return False
        if "official" not in official_info.keys() or "id" not in official_info["official"]:
            return False
        return official_info["official"]["id"]

    def official_is_at_homeplate(self, official_info):
        """Check is the given official was behind the plate for the given game.

        official_info: object - Official's info (e.g. name, id, etc).
        """
        if not super().validate_object(official_info) or "officialType" not in official_info.keys():
            return False
        official_type = official_info["officialType"].strip().lower()
        home_plate_re = re.compile(r"home *plate")
        return home_plate_re.search(official_type)

    def get_id_of_official_at_homeplate(self, list_of_officials):
        """Returns the info of the official who was behind the plate.

        list_of_officials: list - List of dictionaries with each one representing info of an official.
        """
        if not super().validate_list(list_of_officials):
            return False

        id_of_official_at_homeplate = False
        for official_info in list_of_officials:
            if self.official_is_at_homeplate(official_info):
                id_of_official_at_homeplate = self.get_official_id(official_info)

        return id_of_official_at_homeplate

    def convert_date_value(self, date_value):
        try:
            return datetime.strptime(date_value, "%Y-%m-%d").date()
        except:
            return False

    def update_game_data_mapping(self, game_data):
        """Returns the updated game description data.

        game_data: dict - data from which we want to update the date value
        """
        if not super().validate_object(game_data) or "date" not in game_data.keys() or "officialHomeplate" not in game_data.keys():
            return False

        game_data.update({
            "date": self.convert_date_value(game_data["date"]),
            "officialHomeplate": self.get_id_of_official_at_homeplate(game_data["officialHomeplate"])
        })

        return game_data

    def get_games_from_database(self):
        """Returns a list of tuples with each tuple representing the unique contraint values of each record in the games table."""
        if not self.games_table_unique_columns:
            return False
        games_table_object = self.database_handler.get_table_object("games")
        return self.database_handler.get_table_column_values(games_table_object, self.games_table_unique_columns)

    def get_game_mapping_values_for_each_game(self, data_by_game):
        """Returns a list of each game object mapping.

        data_by_game: list - List of dictionaries with each one representing the API data of a game.
        """
        game_data_by_game = super().get_mapping_values(GAMES_MAPPING, data_by_game)
        return False if not game_data_by_game else game_data_by_game

    def update_game_mapping_values(self, game_mapping_values):
        """Returns a list of each game object mapping with updated values.

        game_mapping_values: list - List of dictionaries with each one representing the mapping value of a game.
        """
        if not super().validate_list(game_mapping_values):
            return False

        updated_game_mapping_list = []
        for game_mapping in game_mapping_values:
            updated_game_mapping = self.update_game_data_mapping(game_mapping)
            if super().validate_object(updated_game_mapping):
                updated_game_mapping_list.append(updated_game_mapping)

        return False if not super().validate_list(updated_game_mapping_list) else updated_game_mapping_list

    def init(self, data):
        games_data = self.get_game_mapping_values_for_each_game(data)
        if not games_data:
            return False

        updated_games_data = self.update_game_mapping_values(games_data)
        if not updated_games_data:
            return False

        game_values_converted_to_table_objects = self.convert_games_to_table_objects(updated_games_data)
        if not game_values_converted_to_table_objects:
            return False

        games_already_in_db = self.get_games_from_database()
        games_to_insert = self.remove_objects_already_in_table(game_values_converted_to_table_objects, games_already_in_db,
                self.games_table_unique_columns)

        return False if not super().validate_list(games_to_insert) else games_to_insert
