from scripts.dataHandlers.base import BaseHandler
from mappings.playEvents import PLAY_EVENTS_MAPPING


class PlayEventsDataHandler(BaseHandler):
    def __init__(self):
        super(PlayEventsDataHandler, self).__init__()
        self.play_events_table_unique_columns = self.database_handler.get_table_unique_constraint_column_names("playEvents")

    def create_play_event(self, play_event_info):
        if not super().validate_object(play_event_info):
            return False
        return self.database_handler.create_play_event(play_event_info)

    def convert_play_events_to_table_objects(self, play_events_list):
        """Returns a list of each play event object mapping converted to a table object.

        play_events_list: list - List of dictionaries with each one representing the mapping values of a play event.
        """
        if not super().validate_list(play_events_list):
            return False
        
        converted_play_events_list = []
        for play_event_info in play_events_list:
            play_event_object = self.create_play_event(play_event_info)
            if play_event_object:
                converted_play_events_list.append(play_event_object)

        return False if not super().validate_list(converted_play_events_list) else converted_play_events_list

    def get_play_events_from_database(self):
        """Returns a list of tuples with each tuple representing the unique contraint values of each record in the playEvents table."""
        if not self.play_events_table_unique_columns:
            return False
        play_events_table_object = self.database_handler.get_table_object("playEvents")
        return self.database_handler.get_table_column_values(play_events_table_object, self.play_events_table_unique_columns)

    def get_unique_play_events_list(self, play_events_list):
        """Removes all duplicated play events and returns a list of unique ones.

        play_events_list: list - List of all play event info.
        """
        unique_play_events = []
        unique_event_names = set()

        if not super().validate_list(play_events_list):
            return False
        
        for play_event_info in play_events_list:
            if not super().validate_object(play_event_info) or "eventName" not in play_event_info.keys():
                continue
            
            event_name = play_event_info["eventName"] if "eventName" in play_event_info.keys() else False
            if not event_name:
                continue

            # If the play event's name was found in the unique_event_names set, then disgard it since it is a duplicate.
            if event_name in unique_event_names:
                continue

            unique_play_events.append(play_event_info)
            unique_event_names.add(event_name)

        return False if not super().validate_list(unique_play_events) else unique_play_events

    def get_play_events_mapping_values_for_each_game(self, data_by_game):
        """Returns a list of each matchup object mapping.

        data_by_game: list - List of dictionaries with each one representing the API data of a game.
        """
        all_play_events = []
        for game_data in data_by_game:
            play_events_data = super().get_mapping_values(PLAY_EVENTS_MAPPING, game_data)
            if not play_events_data:
                continue
            all_play_events = all_play_events + play_events_data
        return False if not all_play_events else all_play_events

    def init(self, data):
        all_plays_by_game = super().get_all_play_by_game(data)
    
        play_events_data = self.get_play_events_mapping_values_for_each_game(all_plays_by_game)
        if not play_events_data:
            return False

        unique_play_events_list = self.get_unique_play_events_list(play_events_data)
        if not unique_play_events_list:
            return False

        play_events_values_converted_to_table_objects = self.convert_play_events_to_table_objects(play_events_data)
        if not play_events_values_converted_to_table_objects:
            return False
    
        play_events_already_in_db = self.get_play_events_from_database()
        play_events_to_insert = self.remove_objects_already_in_table(play_events_values_converted_to_table_objects, play_events_already_in_db,
                self.play_events_table_unique_columns)

        return play_events_to_insert
