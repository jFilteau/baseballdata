from scripts.dataHandlers.base import BaseHandler
from mappings.officials import OFFICIALS_MAPPING


class OfficialDataHandler(BaseHandler):
    def __init__(self):
        super(OfficialDataHandler, self).__init__()
        self.officials_table_unique_columns = self.database_handler.get_table_unique_constraint_column_names("officials")

    def create_official(self, official_info):
        if not super().validate_object(official_info):
            return False
        return self.database_handler.create_official(official_info)

    def convert_official_data_to_table_objects(self, officials_list):
        """Returns a list of each official object mapping converted to a table object.

        officials_list: list - List of dictionaries with each one representing the mapping values of an official.
        """
        if not super().validate_list(officials_list):
            return False
        
        converted_officials_list = []
        for official_data in officials_list:
            official_object = self.create_official(official_data)
            if official_object:
                converted_officials_list.append(official_object)

        return False if not super().validate_list(converted_officials_list) else converted_officials_list

    def get_officials_from_database(self):
        """Returns a list of tuples with each tuple representing the unique contraint values of each record in the officials table."""
        if not self.officials_table_unique_columns:
            return False
        officials_table_object = self.database_handler.get_table_object("officials")
        return self.database_handler.get_table_column_values(officials_table_object, self.officials_table_unique_columns)

    def get_list_of_officials_from_game(self, game_data):
        """Returns the list of officials for a given game.

        game_data: dict - data for a given game
        """
        if not super().validate_object(game_data):
            return False

        live_data = super().get_live_data(game_data)
        if not live_data or "boxscore" not in live_data.keys():
            return False

        boxscore = live_data["boxscore"]
        if not super().validate_object(boxscore) or "officials" not in boxscore.keys():
            return False

        officials = boxscore["officials"]
        return False if not super().validate_list(officials) else officials

    def create_list_of_officials_from_all_games(self, data_by_game):
        """Returns the list of officials for a given game.

        data_by_game: list - List of dictionaries with each one representing data for a given game.
        """
        all_officials = []
        officials_data_by_game = list(map(self.get_list_of_officials_from_game, data_by_game))

        for officials_list in officials_data_by_game:
            if not super().validate_list(officials_list):
                continue

            for official_info in officials_list:
                if super().validate_object(official_info):
                    all_officials.append(official_info)
        
        return False if not super().validate_list(all_officials) else all_officials

    def get_unique_officials_list(self, list_of_officials):
        """Removes all duplicated officials and returns a list of unique ones.

        officials_list: list - List of all official info.
        """
        if not super().validate_list(list_of_officials):
            return False
        
        unique_officials = []
        unique_ids = set()

        for official_info in list_of_officials:
            if not super().validate_object(official_info):
                continue
            
            official_id = official_info["id"] if "id" in official_info.keys() else False
            if not official_id:
                continue

            # If the official's id was found in the unique_ids set, then disgard it since it is a duplicate.
            if official_id in unique_ids:
                continue

            unique_officials.append(official_info)
            unique_ids.add(official_id)

        return False if not super().validate_list(unique_officials) else unique_officials

    def get_officials_mapping_values_for_each_game(self, data_by_game):
        """Returns a list of each official object mapping.

        data_by_game: list - List of dictionaries with each one representing the API data of a game.
        """
        official_data_by_game = super().get_mapping_values(OFFICIALS_MAPPING, data_by_game)
        return False if not official_data_by_game else official_data_by_game

    def init(self, data):
        list_of_all_officials = self.create_list_of_officials_from_all_games(data)

        officials_data = self.get_officials_mapping_values_for_each_game(list_of_all_officials)
        if not officials_data:
            return False

        unique_officials_list = self.get_unique_officials_list(officials_data)
        if not unique_officials_list:
            return False

        officials_values_converted_to_table_objects = self.convert_official_data_to_table_objects(unique_officials_list)
        if not officials_values_converted_to_table_objects:
            return False

        matchups_already_in_db = self.get_officials_from_database()
        officials_to_insert = self.remove_objects_already_in_table(officials_values_converted_to_table_objects, matchups_already_in_db,
                self.officials_table_unique_columns)

        return officials_to_insert
