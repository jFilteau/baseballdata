from scripts.dataHandlers.base import BaseHandler
from mappings.teams import TEAM_MAPPING


class TeamDataHandler(BaseHandler):
    def __init__(self):
        super(TeamDataHandler, self).__init__()
        self.teams_table_unique_columns = self.database_handler.get_table_unique_constraint_column_names("teams")

    def create_team(self, team_info):
        if not super().validate_object(team_info):
            return False
        return self.database_handler.create_team(team_info)

    def convert_team_data_to_table_objects(self, teams_list):
        """Returns a list of each team object mapping converted to a table object.

        teams_list: list - List of dictionaries with each one representing the mapping values of a team.
        """
        if not super().validate_list(teams_list):
            return False
        
        converted_teams_list = []
        for team_data in teams_list:
            team_object = self.create_team(team_data)
            if team_object:
                converted_teams_list.append(team_object)

        return False if not super().validate_list(converted_teams_list) else converted_teams_list

    def get_teams_from_database(self):
        """Returns a list of tuples with each tuple representing the unique contraint values of each record in the teams table."""
        if not self.teams_table_unique_columns:
            return False
        teams_table_object = self.database_handler.get_table_object("teams")
        return self.database_handler.get_table_column_values(teams_table_object, self.teams_table_unique_columns)

    def get_division_name_from_team_data(self, team_data):
        """Returns the division name based on the teams's division int value.

        team_data: dict - data from which we want to retrieve the division integer value
        """
        if not super().validate_object(team_data) or "division" not in team_data.keys():
            return False

        division_id = team_data["division"]
        if division_id in [200, 203]:
            return "West"
        elif division_id in [201, 204]:
            return "East"
        elif division_id in [202, 205]:
            return "Central"
        else:
            return False

    def get_league_name_from_team_data(self, team_data):
        """Returns the league name based on the teams's league int value.

        team_data: dict - data from which we want to retrieve the league integer value
        """
        if not super().validate_object(team_data) or "league" not in team_data.keys():
            return False

        league_id = team_data["league"]
        if league_id == 103:
            return "AL"
        elif league_id == 104:
            return "NL"
        else:
            return False

    def update_team_data_mapping(self, team_data):
        """Returns the updated team data.

        team_data: dict - data from which we want to update the league and division values
        """
        division_enum_value = self.get_division_name_from_team_data(team_data)
        league_enum_value = self.get_league_name_from_team_data(team_data)
        if not division_enum_value or not league_enum_value:
            return False

        team_data.update({"division": division_enum_value, "league": league_enum_value})

        return team_data

    def get_teams_mapping_values_for_each_game(self, data_by_game):
        """Returns a list of each team object mapping.

        data_by_game: list - List of dictionaries with each one representing the API data of a game.
        """
        team_data_by_game = super().get_mapping_values(TEAM_MAPPING, data_by_game)
        return False if not team_data_by_game else team_data_by_game

    def update_team_mappings_values(self, team_mapping_values):
        """Returns a list of each team object mapping with updated values.

        team_mapping_values: list - List of dictionaries with each one representing the mapping value of a team.
        """
        if not super().validate_list(team_mapping_values):
            return False

        updated_team_mapping_list = []
        for team_mapping in team_mapping_values:
            updated_team_mapping = self.update_team_data_mapping(team_mapping)
            if super().validate_object(updated_team_mapping):
                updated_team_mapping_list.append(updated_team_mapping)

        return False if not super().validate_list(updated_team_mapping_list) else updated_team_mapping_list


    def init(self, data):
        teams_data = self.get_teams_mapping_values_for_each_game(data)
        if not teams_data:
            return False

        updated_data = self.update_team_mappings_values(teams_data)
        if not updated_data:
            return False

        team_values_converted_to_table_objects = self.convert_team_data_to_table_objects(updated_data)
        if not team_values_converted_to_table_objects:
            return False

        teams_already_in_db = self.get_teams_from_database()
        teams_to_insert = self.remove_objects_already_in_table(team_values_converted_to_table_objects, teams_already_in_db,
                self.teams_table_unique_columns)

        return teams_to_insert
