import copy
from scripts.dataHandlers.base import BaseHandler
from mappings.matchups import MATCHUPS_MAPPING


class MatchupsDataHandler(BaseHandler):
    def __init__(self):
        super(MatchupsDataHandler, self).__init__()
        self.matchups_table_unique_columns = self.database_handler.get_table_unique_constraint_column_names("matchups")

    def create_matchup(self, matchup_info):
        if not super().validate_object(matchup_info):
            return False
        return self.database_handler.create_matchup(matchup_info)

    def convert_matchups_to_table_objects(self, matchups_list):
        """Returns a list of each matchup object mapping converted to a table object.

        matchups_list: list - List of dictionaries with each one representing the matchup mapping values of a game.
        """
        if not super().validate_list(matchups_list):
            return False

        converted_matchups_list = []
        for matchup_data in matchups_list:
            matchup_object = self.create_matchup(matchup_data)
            if matchup_object:
                converted_matchups_list.append(matchup_object)

        return False if not super().validate_list(converted_matchups_list) else converted_matchups_list

    def get_runner_id_from_event(self, runner_event):
        """Returns the id of the runner on a given base.

        runner_event: obj - Object containing the info of the runner on a given base.
        """
        if not super().validate_object(runner_event) or not "details" in runner_event.keys():
            return False

        runner_event_details = runner_event["details"]
        if not super().validate_object(runner_event_details) or "runner" not in runner_event_details.keys():
            return False

        runner_event_info = runner_event_details["runner"]
        if not super().validate_object(runner_event_info) or "id" not in runner_event_info.keys():
            return False

        return runner_event_info["id"]

    def get_runner_start_base_during_at_bat_event(self, runner_event_info):
        """Returns the start base of the runner as the at bat event occured.

        runner_event_info: object - Data of the runner on the given base.
        """
        if not super().validate_object(runner_event_info):
            return False

        if "movement" not in runner_event_info.keys() or "start" not in runner_event_info["movement"].keys():
            return False

        return runner_event_info["movement"]["start"]

    def get_event_type_of_runner_event(self, runner_event_info):
        """Returns the type of event (e.g. stolen base, caught stealing, etc).

        runner_event_info: object - Data of the runner on the given base.
        """
        if not super().validate_object(runner_event_info):
            return False

        if "details" not in runner_event_info.keys() or "eventType" not in runner_event_info["details"].keys():
            return False

        event_type = runner_event_info["details"]["eventType"]
        return False if not event_type else event_type.strip().lower()

    def update_runner_id_by_base_values_based_on_event(self, runner_event_info, runner_id_by_base):
        if not super().validate_object(runner_event_info) or not super().validate_object(runner_id_by_base):
            return {"runner1B": None, "runner2B": None, "runner3B": None}

        movement_start = self.get_runner_start_base_during_at_bat_event(runner_event_info)
        if movement_start is None:
            return runner_id_by_base

        event_type = self.get_event_type_of_runner_event(runner_event_info)
        if not event_type or not movement_start:
            return runner_id_by_base

        if event_type == "stolen_base_2b":
            runner_id_by_base.update({"runner1B": None, "runner2B": self.get_runner_id_from_event(runner_event_info)})
        elif event_type == "stolen_base_3b":
            runner_id_by_base.update({"runner2B": None, "runner3B": self.get_runner_id_from_event(runner_event_info)})
        else:
            if movement_start == "1B":
                runner_id_by_base["runner1B"] = self.get_runner_id_from_event(runner_event_info)
            elif movement_start == "2B":
                runner_id_by_base["runner2B"] = self.get_runner_id_from_event(runner_event_info)
            elif movement_start == "3B":
                runner_id_by_base["runner3B"] = self.get_runner_id_from_event(runner_event_info)

        return runner_id_by_base

    def update_matchup_data_runners(self, matchup_data):
        """Returns the id of the runner on a given base.

        runner_event: obj - Object containing the info of the runner on a given base.
        """
        if not super().validate_object(matchup_data):
            return False

        runner_id_by_base = {"runner1B": None, "runner2B": None, "runner3B": None}

        # When doing the mapping extraction from the path list, each runner_B key fetched the same list of runner events, therefore
        # we can pick anyone of them to get the event list. We'll use runner1B.
        runner_events = matchup_data["runner1B"]

        # An empty list means there were no runners on base during the atbat, so simply replace each runner_B value with None.
        if not super().validate_list(runner_events):
            return matchup_data.update(runner_id_by_base)

        for runner_event in runner_events:
            runner_id_by_base = self.update_runner_id_by_base_values_based_on_event(runner_event, runner_id_by_base)

        matchup_data.update(runner_id_by_base)
        return matchup_data

    def get_last_play_event(self, play_events):
        """Returns the last event, i.e. last pitch, of an at bat.

        play_events: array - List of all pitch data for a given at bat.
        """
        if not super().validate_list(play_events):
            return False
        return play_events[-1]

    def get_play_id_of_last_play_event(self, play_events):
        """
        Returns the play id of the last event, i.e. last pitch, of an at bat. This id will be used when retrieving single
        pitch videos.

        play_events: array - List of all pitch data for a given at bat.
        """
        if not super().validate_list(play_events):
            return False

        last_play_event = self.get_last_play_event(play_events)
        if not super().validate_object(last_play_event) or not "playId" in last_play_event.keys():
            return False

        return last_play_event["playId"]

    def update_matchup_hit_data(self, matchup_data, play_events):
        if not super().validate_object(matchup_data) or not super().validate_list(play_events):
            return False

        last_play_event = self.get_last_play_event(play_events)
        hit_data_key_to_path = {
            "hitLaunchSpeed"  : "launchSpeed",
            "hitLaunchAngle"  : "launchAngle",
            "hitTotalDistance": "totalDistance",
            "hitTrajectory"   : "trajectory",
            "hitHardness"     : "hardness",
            "hitLocation"     : "location",
            "hitCoordinateX"  : "coordX",
            "hitCoordinateY"  : "coordY"
        }

        for key, path in hit_data_key_to_path.items():
            # The last play event, i.e. last pitch, of the at bat is when the hit was accomplished.
            # If the hitData key does not exists, that means the ball was not put in play and there is not hit data to retrieve.
            if not super().validate_object(last_play_event) or not "hitData" in last_play_event.keys():
                matchup_data[key] = False
                continue

            at_bat_hit_data = last_play_event["hitData"]
            if not super().validate_object(at_bat_hit_data):
                matchup_data[key] = False
                continue

            # We'll pull the coordinates data back one key so that all the hit data are on the same level.
            # e.g. If we have { hitDistance: 200, coordinates: { coordX: 2, coordY: 5 } }, we'll convert it to
            # { hitDistance: 200, coordX: 2, coordY: 5 }
            if "coordinates" in at_bat_hit_data.keys():
                at_bat_hit_data.update(at_bat_hit_data["coordinates"])

            matchup_data[key] = at_bat_hit_data.pop(path, False)

        return matchup_data

    def update_matchup_data_mapping(self, matchup_data):
        """Returns the updated matchup data.

        matchup_data: dict - data we want to update
        """
        if not super().validate_object(matchup_data):
            return False

        play_events = matchup_data.pop("playEvents", False)
        if not play_events:
            return False

        matchup_data["playId"] = self.get_play_id_of_last_play_event(play_events)
        matchup_data           = self.update_matchup_data_runners(matchup_data)
        matchup_data           = self.update_matchup_hit_data(matchup_data, play_events)

        return matchup_data

    def update_matchups_mapping_values(self, matchup_mapping_values):
        """Returns a list of each matchup object mapping with updated values.

        matchup_mapping_values: list - List of dictionaries with each one representing the mapping value of a matchup.
        """
        if not super().validate_list(matchup_mapping_values):
            return False

        updated_mapping_list = []
        for matchup_mapping in matchup_mapping_values:
            updated_matchup_mapping = self.update_matchup_data_mapping(matchup_mapping)
            if super().validate_object(updated_matchup_mapping):
                updated_mapping_list.append(updated_matchup_mapping)

        return False if not super().validate_list(updated_mapping_list) else updated_mapping_list

    def get_matchups_from_database(self):
        """Returns a list of tuples with each tuple representing the unique contraint values of each record in the matchups table."""
        if not self.matchups_table_unique_columns:
            return False
        matchups_table_object = self.database_handler.get_table_object("matchups")
        return self.database_handler.get_table_column_values(matchups_table_object, self.matchups_table_unique_columns)

    def matchup_had_pitching_change_mid_count(self, matchup_play_events):
        """Returns true if a pitching change was made in the middle of a count.

        matchup_info: dict - Data for a given at bat matchup.
        """
        if not super().validate_list(matchup_play_events):
            return False

        # We have to keep count of the pitches made before the pitching change because it's possible that the subsitution
        # is the first value in the play events list, in which case the change was made before any pitch was thrown by the
        # previous pitcher and we don't need to create an extra matchup record for the previous pitcher.
        pitch_count = 0

        for event in matchup_play_events:
            try:
                if event["isPitch"]:
                    pitch_count += 1
                elif int(event["position"]["code"]) == 1:
                    # Making sure that the pitching change was made mid-count, i.e. that at least 1 pitch was thrown prior.
                    if pitch_count > 0:
                        return True
            except:
                continue

        return False

    def get_list_of_all_game_matchups(self, game_data):
        """
        Returns a list of all the matchups, i.e. at bats, in a game. This function's goal is to add any additional
        matchup in which a pitching change was made in the middle of a count.
        e.g. If pitcher A gets taken out of an at bat for pitcher B on a 3-1 count, then the matchup data will only
        include pitcher B. Therefore, we need to add the additional data of the 4 pitches made by pitcher A.

        game_data: dict - List of data for a given game.
        """
        if not super().validate_object(game_data):
            return False

        matchups_data_list = super().get_plays_data_of_game(game_data)
        if not super().validate_list(matchups_data_list):
            return False

        # The away and home pitcher values will get updated as we loop the plays. The reason for creating these variables
        # is for at bats where a pitching change is done but pitches were already made. The matchup data for an at bat
        # only states which pitcher ended the at bat so by keeping track of the pitcher we'll be able to assign the
        # pitches accordingly instead of associating all of them to the pitcher that ended the at bat.
        current_away_pitcher = None
        current_home_pitcher = None
        def update_current_pitcher(pitcher_id, inning):
            if inning == "top":
                nonlocal current_home_pitcher
                current_home_pitcher = pitcher_id
            else:
                nonlocal current_away_pitcher
                current_away_pitcher = pitcher_id

        all_matchups_for_given_game = []
        for matchup_info in matchups_data_list:
            # We'll append the current matchup since it's the matchup that ended the at bat.
            all_matchups_for_given_game.append(matchup_info)

            try:
                inning               = matchup_info["about"]["inning"]
                half_inning          = matchup_info["about"]["halfInning"]
                pitcher_id           = matchup_info["matchup"]["pitcher"]["id"]
                matchup_play_events  = matchup_info["playEvents"]
            except:
                continue

            # It's important to initiate the starting pitcher in case a pitcher was substituted out in the first at bat (Yikes...).
            if inning == 1:
                update_current_pitcher(pitcher_id, half_inning)

            matchup_has_pitching_change = self.matchup_had_pitching_change_mid_count(matchup_play_events)
            if matchup_has_pitching_change:
                # We will loop again to see if a pitching change was made before any pitch was throw so
                # that we can update the current pitcher.
                # e.g. If the current pitcher is 111 and a pitching change was made at the begining of the
                # at bat to put pitcher 222 in, then the pitches thrown before the mid-count pitching change
                # belongs to pitcher 222 and not 111.
                for event in matchup_play_events:
                    try:
                        # Once a pitch is thrown, we know for certain that the current pitcher values are valid
                        # so we don't need to continue the loop any further.
                        if event["isPitch"]:
                            break

                        # If a pitching change was made before any pitches were thrown, then we must update
                        # the current pitcher value since the following pitches will belong to the new pitcher.
                        if int(event["position"]["code"]) == 1:
                            update_current_pitcher(event["player"]["id"], half_inning)
                    except:
                        continue

                # Very important to make a deep copy or else the modifications under will also affect the original dict.
                additional_matchup = copy.deepcopy(matchup_info)
                additional_matchup["matchup"]["pitcher"]["id"] = current_home_pitcher if half_inning == "top" else current_away_pitcher
                additional_matchup["result"]["event"] = "Pitching Substitution"
                all_matchups_for_given_game.append(additional_matchup)

            # Make sure to update the current pitcher at the end of each matchup so that we know who threw the pitches
            # before a mid-count at bat pitching change (if it happened).
            update_current_pitcher(pitcher_id, half_inning)

        return all_matchups_for_given_game

    def create_list_of_all_matchup_at_bats(self, data_by_game):
        """
        Returns an array of dicts with each one containing information for a single at bat.

        data_by_game: array of dict - Each dict contains all necessary information for a single game.
        """
        if not super().validate_list(data_by_game):
            return False

        list_of_all_matchups = []
        for game_data in data_by_game:
            game_matchups = self.get_list_of_all_game_matchups(game_data)
            if game_matchups:
                list_of_all_matchups.extend(game_matchups)

        return False if not super().validate_list(list_of_all_matchups) else list_of_all_matchups

    def init(self, data_of_all_games):
        list_of_all_matchup_at_bats = self.create_list_of_all_matchup_at_bats(data_of_all_games)
        if not list_of_all_matchup_at_bats:
            return False

        converted_matchup_mappings = super().get_mapping_values(MATCHUPS_MAPPING, list_of_all_matchup_at_bats)
        if not converted_matchup_mappings:
            return False

        updated_matchup_mappings = self.update_matchups_mapping_values(converted_matchup_mappings)
        if not updated_matchup_mappings:
            return False

        matchups_converted_to_table_objects = self.convert_matchups_to_table_objects(updated_matchup_mappings)
        if not matchups_converted_to_table_objects:
            return False

        matchups_already_in_db = self.get_matchups_from_database()
        # False means there are no records in the matchups table, therefore we can create all the converted matchups.
        if not matchups_already_in_db:
            return matchups_converted_to_table_objects

        matchups_to_insert = self.remove_objects_already_in_table(matchups_converted_to_table_objects, matchups_already_in_db,
                self.matchups_table_unique_columns)

        return matchups_to_insert
