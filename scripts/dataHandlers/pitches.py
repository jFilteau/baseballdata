from database.tables.pitchFx import CallCodeEnum
from scripts.dataHandlers.base import BaseHandler
from mappings.pitchFx import PITCHFX_MAPPINGS


class PitchesDataHandler(BaseHandler):
    def __init__(self):
        super(PitchesDataHandler, self).__init__()
        self.pitches_table_unique_columns = self.database_handler.get_table_unique_constraint_column_names("pitchFx")
        self.matchups_unique_constraint_values_to_pks = self.fetch_all_matchup_unique_constraint_values_and_primary_keys()

    def fetch_all_matchup_unique_constraint_values_and_primary_keys(self):
        """Returns a dict containing the unique constraint values of each matchup row and the row's primary key as the associated value"""
        matchups_table_object                    = self.database_handler.get_table_object("matchups")
        matchups_table_unique_constraint_columns = self.database_handler.get_table_unique_constraint_column_names("matchups")

        # Append the id column since it's not part of the table's unique constraint but is needed since it's the table's pk.
        matchups_table_unique_constraint_columns.append("id")

        unique_constraint_values = self.database_handler.get_table_column_values(matchups_table_object, matchups_table_unique_constraint_columns)

        # Creating a dict with the unique constraint value of a given row, i.e. (gamePk, atBatGameIndex, pitcherId, batterId), as key
        # and the row's primary key as the associated value.
        unique_contraint_value_to_pk = {}
        for values in unique_constraint_values:
            game_pk, at_bat_game_index, pitcher_id, batter_id, primary_key = values
            unique_contraint_value_to_pk[(game_pk, at_bat_game_index, pitcher_id, batter_id)] = primary_key

        return unique_contraint_value_to_pk

    def create_pitch(self, pitch_info):
        if not super().validate_object(pitch_info):
            return False
        return self.database_handler.create_pitch(pitch_info)

    def get_call_code_enum_value(self, pitch_data):
        """Returns the codei value associated to the three possible events of a pitch.

        pitch_data: dict - Data from which we want to retrieve the call code
        """
        if not super().validate_object(pitch_data):
            return False

        try:
            is_in_play = pitch_data.pop("isInPlay", False)
            is_ball    = pitch_data.pop("isBall", False)
            is_strike  = pitch_data.pop("isStrike", False)

            if is_in_play:
                return CallCodeEnum["InPlay"].name
            elif is_ball:
                return CallCodeEnum["Ball"].name
            elif is_strike:
                return CallCodeEnum["Strike"].name
            else:
                return False

        except:
            return False


    def update_pitch_data_mapping(self, pitch_data):
        """Returns the updated pitch data.

        pitch_data: dict - data from which we want to update the position value
        """
        if not super().validate_object(pitch_data):
            return False

        try:
            call_code = pitch_data["callCode"].strip().lower()
            # Skip intentional walks
            if call_code == "v":
                return False

            call_code_enum_value = self.get_call_code_enum_value(pitch_data)
            # Skip invalid enum values
            if not call_code_enum_value:
                return False

            # Required values to fetch the matchup foreign key. We'll pop them out of the dict because they are not needed
            # in the creation of the pitch table object.
            game_pk           = pitch_data.pop("gamePk", False)
            at_bat_game_index = pitch_data.pop("atBatGameIndex", False)
            pitcher_id        = pitch_data.pop("pitcherId", False)
            batter_id         = pitch_data.pop("batterId", False)

            # If the matchup id retrieval throws an error, that means we can't add a matchup foreign key and we'll return False
            # via the exception handler bellow.
            matchup_id = self.matchups_unique_constraint_values_to_pks[(game_pk, at_bat_game_index, pitcher_id, batter_id)]

            pitch_data["call"]      = call_code_enum_value
            pitch_data["matchupId"] = matchup_id

            return pitch_data

        except:
            return False

    def get_pitches_from_database(self):
        """Returns a list of tuples with each tuple representing the unique contraint values of each record in the pitchFx table."""
        if not self.pitches_table_unique_columns:
            return False
        pitches_table_object = self.database_handler.get_table_object("pitchFx")
        return self.database_handler.get_table_column_values(pitches_table_object, self.pitches_table_unique_columns)

    def matchup_had_pitching_change_mid_count(self, play_events):
        """Returns true if a pitching change was made in the middle of a count.

        play_events: array - List of all events that happended during the at bat.
        """
        if not super().validate_list(play_events):
            return False

        # We have to keep count of the pitches made before the pitching change because it's possible that the subsitution
        # is the first value in the play events list, in which case the change was made before any pitch was thrown by the
        # previous pitcher and we don't need to create an extra matchup record for the previous pitcher.
        pitch_count = 0

        for event in play_events:
            try:
                if event["isPitch"]:
                    pitch_count += 1

                # The pitcher has code 1 on defense.
                elif int(event["position"]["code"]) == 1:
                    # Making sure that the pitching change was made mid-count, i.e. that at least 1 pitch was thrown prior.
                    if pitch_count > 0:
                        return True
            except:
                continue

        return False

    def get_pitch_data_for_each_matchup(self, all_plays_data):
        """Returns a list of dicts with each one representing data for a single pitch.

        all_plays_data: array - List of data for a given game.
        """
        if not super().validate_list(all_plays_data):
            return False

        # The outs count gets updated after every matchup (it can remain the same value if the at bat resulted in a hit and no
        # was was thrown out on the bases).
        current_outs_count = 0

        # The away and home pitcher values will get updated as we loop the plays. The reason for creating these variables
        # is for at bats where a pitching change is done but pitches were already made. The matchup data for an at bat
        # only states which pitcher ended the at bat so by keeping track of the pitcher we'll be able to assign the
        # pitches accordingly instead of associating all of them to the pitcher that ended the at bat.
        current_away_pitcher = None
        current_home_pitcher = None
        def update_current_pitcher(pitcher_id, inning):
            if inning == "top":
                nonlocal current_home_pitcher
                current_home_pitcher = pitcher_id
            else:
                nonlocal current_away_pitcher
                current_away_pitcher = pitcher_id

        pitches_list = []
        for matchup_info in all_plays_data:
            try:
                # Required values to process the matchup.
                game_pk      = matchup_info["gamePk"]
                at_bat_index = matchup_info["atBatIndex"]
                play_events  = matchup_info["playEvents"]
                half_inning  = matchup_info["about"]["halfInning"].strip().lower()
                inning       = int(matchup_info["about"]["inning"])
                batter_id    = int(matchup_info["matchup"]["batter"]["id"])
                pitcher_id   = int(matchup_info["matchup"]["pitcher"]["id"])
            except:
                continue

            # An at bat always starts with an 0-0 count.
            previous_pitch_strikes = 0
            previous_pitch_balls   = 0

            # It's important to initiate the starting pitcher or else the pitcher value will be None until there is a pitching change.
            if inning == 1:
                update_current_pitcher(pitcher_id, half_inning)

            # We can change the current pitcher if there were no pitching changes made in the at bat since we know that
            # we won't need to create an extra matchup record.
            matchup_has_pitching_change_mid_count = self.matchup_had_pitching_change_mid_count(play_events)
            if not matchup_has_pitching_change_mid_count:
                update_current_pitcher(pitcher_id, half_inning)

            for event in play_events:
                try:
                    if not event["isPitch"]:
                        # If the code is 1, i.e. the pitcher's code number of defense, then there was a pitching change
                        # and we have to update the current pitcher. It's very important to do this step since it
                        # happens that pitchers get taken out in the middle of a count and the matchup data only includes
                        # the pitcher who finished the at bat.
                        if int(event["position"]["code"]) == 1:
                            update_current_pitcher(event["player"]["id"], half_inning)
                            continue

                        # An out made on a non-pitch means a runner got picked off at a base. In this case all we
                        # do is increment the outs count.
                        if "outs" in event["count"]:
                            current_outs_count = int(event["count"]["outs"])
                            continue
                except:
                    continue

                # We have to manually update the pitch event info with extra data needed for the db.
                event["gamePk"]                  = game_pk
                event["atBatGameIndex"]          = at_bat_index
                event["ballsCountBeforePitch"]   = previous_pitch_balls
                event["strikesCountBeforePitch"] = previous_pitch_strikes
                event["outsCountBeforePitch"]    = current_outs_count
                event["batterId"]                = batter_id
                event["pitcherId"]               = current_home_pitcher if half_inning == "top" else current_away_pitcher

                try:
                    # We have to update the count for reference on the next pitch, since we're interested in knowing
                    # what the count was before a pitch was made.
                    previous_pitch_strikes = event["count"]["strikes"]
                    previous_pitch_balls   = event["count"]["balls"]
                except:
                    continue

                pitches_list.append(event)

            # Important to compute the modulo so that the number of outs resets after three outs for the next matchup.
            current_outs_count = (current_outs_count + self.number_of_outs_made_in_matchup(matchup_info)) % 3

        return False if not super().validate_list(pitches_list) else pitches_list

    def number_of_outs_made_in_matchup(self, matchup_info):
        """Returns the number of outs made in a matchup. The lowest value is 0, i.e. no outs made, and highest is
        3, i.e. the inning ended at the end of the matchup.

        matchup_info: dict - Data for a given at bat matchup.
        """
        if not super().validate_object(matchup_info) or "runners" not in matchup_info.keys():
            return False

        number_of_outs_made = 0

        # We track what happened to every runner on base, including the batter, at the end of the play to know
        # how many outs were made.
        for runner_info in matchup_info["runners"]:
            if "movement" not in runner_info.keys() or "isOut" not in runner_info["movement"]:
                continue
            if runner_info["movement"]["isOut"] is True:
                number_of_outs_made += 1

        return number_of_outs_made

    def create_list_of_all_pitches(self, data_by_game):
        """Returns an array of dicts with each one containing information for a single pitch.

        data_by_game: array of dict - Each dict contains all necessary information for a single game.
        """
        if not super().validate_list(data_by_game):
            return False

        list_of_all_pitches = []
        for game_data in data_by_game:
            matchups = super().get_plays_data_of_game(game_data)
            if not matchups:
                continue

            pitches = self.get_pitch_data_for_each_matchup(matchups)
            if not pitches:
                continue

            # Make sure to extend instead of append since we want a 1 dimensional array.
            list_of_all_pitches.extend(pitches)

        return False if not super().validate_list(list_of_all_pitches) else list_of_all_pitches

    def update_pitch_mappings(self, all_pitches):
        """Returns an array of dicts with each one representing an updated pitch data.

        all_pitches: array of dict - Each dict contains all necessary information for a single pitch.
        """
        if not super().validate_list(all_pitches):
            return False

        updated_mapping_values = []
        for mapping in all_pitches:
            updated_mapping = self.update_pitch_data_mapping(mapping)
            if updated_mapping:
                updated_mapping_values.append(updated_mapping)

        return False if not super().validate_list(updated_mapping_values) else updated_mapping_values

    def convert_pitches_to_table_object(self, pitch_mappings):
        """Returns an array of each pitch converted to an sqlalchemy Table object.

        pitch_mappings: array of dict - Each dict contains all necessary information for a single pitch.
        """
        if not super().validate_list(pitch_mappings):
            return False

        converted_pitch_list = []
        for mapping in pitch_mappings:
            pitch_object = self.create_pitch(mapping)
            if pitch_object:
                converted_pitch_list.append(pitch_object)

        return False if not super().validate_list(converted_pitch_list) else converted_pitch_list

    def init(self, data):
        list_of_all_pitches = self.create_list_of_all_pitches(data)
        if not list_of_all_pitches:
            return False

        mapping_values_of_pitches = super().get_mapping_values(PITCHFX_MAPPINGS, list_of_all_pitches)
        if not mapping_values_of_pitches:
            return False

        updated_pitch_mappings = self.update_pitch_mappings(mapping_values_of_pitches)
        if not updated_pitch_mappings:
            return False

        converted_pitches_to_table_objects = self.convert_pitches_to_table_object(updated_pitch_mappings)
        if not converted_pitches_to_table_objects:
            return False

        pitches_already_in_db = self.get_pitches_from_database()
        # False means there are no records in the pitchFx table, therefore we can create all the converted pitches.
        if not pitches_already_in_db:
            return converted_pitches_to_table_objects

        pitches_to_insert = self.remove_objects_already_in_table(converted_pitches_to_table_objects, pitches_already_in_db,
                self.pitches_table_unique_columns)

        return pitches_to_insert
