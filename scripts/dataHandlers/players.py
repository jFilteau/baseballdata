from scripts.dataHandlers.base import BaseHandler
from database.tables.players import PositionEnum
from mappings.players import PLAYERS_MAPPING


class PlayerDataHandler(BaseHandler):
    def __init__(self):
        super(PlayerDataHandler, self).__init__()
        self.players_table_unique_columns = self.database_handler.get_table_unique_constraint_column_names("players")

    def create_player(self, player_info):
        if not super().validate_object(player_info):
            return False
        return self.database_handler.create_player(player_info)

    def convert_player_data_to_table_objects(self, players_list):
        """Returns a list of each player object mapping converted to a table object.

        players_list: list - List of dictionaries with each one representing the mapping values of a player.
        """
        if not super().validate_list(players_list):
            return False
        
        converted_players_list = []
        for player_data in players_list:
            player_object = self.create_player(player_data)
            if player_object:
                converted_players_list.append(player_object)

        return False if not super().validate_list(converted_players_list) else converted_players_list

    def get_players_from_database(self):
        """Returns a list of tuples with each tuple representing the unique contraint values of each record in the players table."""
        if not self.players_table_unique_columns:
            return False
        players_table_object = self.database_handler.get_table_object("players")
        return self.database_handler.get_table_column_values(players_table_object, self.players_table_unique_columns)

    def get_position_name_from_player_data(self, player_data):
        """Returns the position name of the player.

        player_data: dict - data from which we want to retrieve the position
        """
        if not super().validate_object(player_data) or "position" not in player_data.keys():
            return "Unknown"

        player_position = player_data["position"].strip().lower()
        for position_fullname, position_abbreviation in PositionEnum.__members__.items():
            if player_position == position_abbreviation.value.strip().lower():
                return position_fullname

        return "Unknown"

    def get_list_of_players_from_game(self, game_data):
        """Returns the list of players who were part of a game.

        game_data: dict - Data for a given game.
        """
        if not super().validate_object(game_data) or "gameData" not in game_data.keys():
            return False
        
        game_data = game_data["gameData"]
        if not super().validate_object(game_data) or "players" not in game_data.keys():
            return False
        
        players_object = game_data["players"]
        return False if not super().validate_object(players_object) else players_object 

    def get_boxscore_data_from_live_data(self, live_data):
        """Returns the boxscore of the game.

        live_data: dict - Dictionary of the game's live data.
        """
        if not super().validate_object(live_data) or "boxscore" not in live_data.keys():
            return False
        return live_data["boxscore"]

    def get_teams_data_from_boxscore_data(self, boxscore_data):
        """Returns the list of teams that were part of the game.

        boxscore_data: dict - Dictionary of the game's boxscore data.
        """
        if not super().validate_object(boxscore_data) or "teams" not in boxscore_data.keys():
            return False
        return boxscore_data["teams"]

    def get_home_team_data_from_teams_data(self, teams_data):
        """Returns the home team's data.

        team_data: dict - Dictionary of team's data.
        """
        if not super().validate_object(teams_data) or "home" not in teams_data.keys():
            return False
        return teams_data["home"]

    def get_away_team_data_from_teams_data(self, teams_data):
        """Returns the away team's data.

        team_data: dict - Dictionary of team's data.
        """
        if not super().validate_object(teams_data) or "away" not in teams_data.keys():
            return False
        return teams_data["away"]

    def get_team_id(self, team_data):
        """Returns the team's unique ID.

        team_data: dict - Dictionary of team's data.
        """
        if not super().validate_object(team_data) or "team" not in team_data.keys():
            return False
        team = team_data["team"]
        return False if "id" not in team.keys() else team["id"]

    def get_team_players(self, team_data):
        """Returns the list of a team's roster.

        team_data: dict - Dictionary of team's data.
        """
        if not super().validate_object(team_data) or "players" not in team_data.keys():
            return False
        return team_data["players"]

    def get_players_list_for_each_team_from_live_data(self, live_data):
        """Returns the roster list of each team.

        live_data: dict - Live data of the game
        """
        dict_team_id_to_roster_list_ids = {}

        boxscore = self.get_boxscore_data_from_live_data(live_data)
        if not super().validate_object(boxscore):
            return False

        teams = self.get_teams_data_from_boxscore_data(boxscore)
        if not super().validate_object(teams):
            return False

        home_team_data = self.get_home_team_data_from_teams_data(teams)
        away_team_data = self.get_away_team_data_from_teams_data(teams)
        if not super().validate_object(home_team_data) or not super().validate_object(away_team_data):
            return False

        away_team_id = self.get_team_id(away_team_data)
        home_team_id = self.get_team_id(home_team_data)
        if not away_team_id or not home_team_id:
            return False

        away_team_players = self.get_team_players(away_team_data)
        home_team_players = self.get_team_players(home_team_data)
        if not away_team_players or not home_team_players:
            return False

        dict_team_id_to_roster_list_ids[away_team_id] = [player_id for player_id, player_info in away_team_players.items()]
        dict_team_id_to_roster_list_ids[home_team_id] = [player_id for player_id, player_info in home_team_players.items()]

        return dict_team_id_to_roster_list_ids

    def get_players_involved_in_game(self, game_data):
        """Returns the list of all the players that were involved in the game, whether they played or were benched.

        game_data: dict - Data for a sngle game.
        """
        if not super().validate_object(game_data):
            return False

        players_list = self.get_list_of_players_from_game(game_data)
        if not super().validate_object(players_list):
            return False

        live_data = super().get_live_data(game_data)
        if not live_data:
            return False

        dict_team_id_to_roster_list = self.get_players_list_for_each_team_from_live_data(live_data)
        if not super().validate_object(dict_team_id_to_roster_list):
            return False

        players_involved_in_game = []
        for player_id, player_info in players_list.items():
            for team_id, team_roster_list in dict_team_id_to_roster_list.items():
                if player_id in team_roster_list:
                    player_info["teamId"] = team_id
                    players_involved_in_game.append(player_info)

        return False if not super().validate_list(players_involved_in_game) else players_involved_in_game

    def update_player_data_mapping(self, player_data):
        """Returns the updated player data.

        player_data: dict - data from which we want to update the position value
        """
        player_data.update({"position": self.get_position_name_from_player_data(player_data)})
        return player_data

    def get_player_mapping_values_for_each_game(self, data_by_game):
        """Returns a list of each player object mapping.

        data_by_game: list - List of dictionaries with each one representing the API data of a game.
        """
        players_data = list(map(self.get_players_involved_in_game, data_by_game))
        players_data = [player_info for players_list in players_data for player_info in players_list]
        mapping_data_by_game = super().get_mapping_values(PLAYERS_MAPPING, players_data)
        mapping_data_by_game = [data for data in mapping_data_by_game if data is not False]
        return False if not mapping_data_by_game else mapping_data_by_game

    def init(self, data):
        players_data = self.get_player_mapping_values_for_each_game(data)
        if not players_data:
            return False

        updated_players_data = list(map(self.update_player_data_mapping, players_data))
        if not updated_players_data:
            return False

        player_values_converted_to_table_objects = self.convert_player_data_to_table_objects(updated_players_data)
        if not player_values_converted_to_table_objects:
            return False

        players_already_in_db = self.get_players_from_database()
        players_to_insert = self.remove_objects_already_in_table(player_values_converted_to_table_objects, players_already_in_db,
                self.players_table_unique_columns)

        return players_to_insert
