import enum
from sqlalchemy.orm import sessionmaker, exc as orm_exc
from sqlalchemy import create_engine, exc as alch_exc
from sqlalchemy_utils import database_exists, create_database
from database.tables.base import Base
from database.tables.teams import Teams
from database.tables.officials import Officials
from database.tables.playEvents import PlayEvents
from database.tables.players import Players
from database.tables.games import Games
from database.tables.matchups import Matchups
from database.tables.pitchFx import PitchFx
from database.tables.boxscores import Boxscores
from shared.dataValidators import DataValidators


class DatabaseHandler:
    def __init__(self):
        self.db_name = "baseballData"
        self.__engine = self.create_engine()
        self.__session = self.create_session()

    def create_engine(self):
        engine = create_engine("mysql+pymysql://justin@localhost/" + self.db_name)
        if not database_exists(engine.url):
            create_database(engine.url)
        return False if not database_exists(engine.url) else engine

    def create_session(self):
        if not self.__engine:
            return False
        session_maker = sessionmaker(bind=self.__engine)
        return session_maker()
    
    def close_session(self):
        if self.__session:
            self.__session.close()

    def data_has_required_keys(self, data, table):
        data_keys = data.keys()
        table_required_keys = self.get_table_column_names(table)

        for required_key in table_required_keys:
            if required_key not in data_keys:
                return False

        return True

    def create_official(self, data):
        if not DataValidators.object_is_valid(data) or not self.data_has_required_keys(data, Officials):
            return False
        return Officials(**data)

    def create_play_event(self, data):
        if not DataValidators.object_is_valid(data) or not self.data_has_required_keys(data, PlayEvents):
            return False
        return PlayEvents(**data)

    def create_game(self, data):
        if not DataValidators.object_is_valid(data) or not self.data_has_required_keys(data, Games):
            return False
        return Games(**data)

    def create_team(self, data):
        if not DataValidators.object_is_valid(data) or not self.data_has_required_keys(data, Teams):
            return False
        return Teams(**data)

    def create_player(self, data):
        if not DataValidators.object_is_valid(data) or not self.data_has_required_keys(data, Players):
            return False
        return Players(**data)

    def create_matchup(self, data):
        if not DataValidators.object_is_valid(data) or not self.data_has_required_keys(data, Matchups):
            return False
        return Matchups(**data)

    def create_pitch(self, data):
        if not DataValidators.object_is_valid(data) or not self.data_has_required_keys(data, PitchFx):
            return False
        return PitchFx(**data)

    def create_boxscore(self, data):
        if not DataValidators.object_is_valid(data) or not self.data_has_required_keys(data, Boxscores):
            return False
        return Boxscores(**data)

    def remove_autoincremented_primary_key_columns(self, list_of_columns):
        column_is_valid = lambda col: not col.primary_key and not col.autoincrement
        return list(filter(column_is_valid, list_of_columns))

    def get_table_column_names(self, table):
        columns = table.__table__.columns
        columns_without_autoincremented_primary_keys = self.remove_autoincremented_primary_key_columns(columns)
        column_names = [col.name for col in columns_without_autoincremented_primary_keys]
        return False if not DataValidators.list_is_valid(column_names) else column_names

    def convert_enum_value(self, value):
        return value.name if isinstance(value, enum.Enum) else value
    
    def convert_query_result_into_list_of_tuples(self, results, unique_columns):
        result_list = []
        for result in results:
            result_tuple = tuple([getattr(result, column_name) for column_name in unique_columns])
            result_tuple = tuple(map(self.convert_enum_value, result_tuple))
            result_list.append(result_tuple)
        return result_list

    def get_table_column_values(self, table, column_names):
        if not table or not DataValidators.list_is_valid(column_names) or not self.__session:
            return False

        try:
            table_name_objects = [table.__table__.c[column_name] for column_name in column_names]
        except KeyError:
            return False

        all_results = self.__session.query(*table_name_objects).distinct().all()
        if not DataValidators.list_is_valid(all_results):
            return False

        query_results_as_list_of_tuples = self.convert_query_result_into_list_of_tuples(all_results, column_names)
        if not DataValidators.list_is_valid(query_results_as_list_of_tuples):
            return False

        return set(query_results_as_list_of_tuples)

    def get_table_object(self, table_name):
        switcher = {
            "players"   : Players,
            "teams"     : Teams,
            "officials" : Officials,
            "playEvents": PlayEvents,
            "matchups"  : Matchups,
            "games"     : Games,
            "pitchFx"   : PitchFx,
            "boxscores" : Boxscores
        }
        return switcher.get(table_name, False)

    def get_table_unique_constraint_column_names(self, table_name):
        switcher = {
            "players"   : ["id", "position", "teamId"],
            "teams"     : ["id"],
            "officials" : ["id"],
            "playEvents": ["eventName"],
            "matchups"  : ["gamePk", "atBatGameIndex", "pitcherId", "batterId"],
            "games"     : ["gamePk", "officialHomeplate", "awayTeam", "homeTeam"],
            "pitchFx"   : ["matchupId", "pitchNumber"],
            "boxscores" : ["gamePk"]
        }
        return switcher.get(table_name, False)

    def bulk_insert_in_db(self, objects):
        if self.__session and objects:
            try:
                self.__session.add_all(objects)
                self.__session.commit()
            except (alch_exc.IntegrityError, orm_exc.FlushError) as e:
                raise Exception(e)
            except Exception as e:
                raise Exception(e)

    def get_missing_launch_speeds(self, start_date, end_date):
        """Returns a list of proxies containing information about the
        rows that have a missing launch speed value despite the ball
        being put in play.

        start_date : string - String of date in the format YYYY/MM/DD
        end_date   : string - String of date in the format YYYY/MM/DD
        """
        if not start_date or not end_date:
            return False

        query = """SELECT *
                FROM pitchData as p
                WHERE p.call = 'InPlay'
                AND p.hitLaunchSpeed = 0
                AND p.date between '{start}' AND '{end}';""".format(start=start_date, end=end_date)

        proxy_results = False
        with self.__engine.connect() as con:
            try:
                proxy = con.execute(query)
                proxy_results = proxy.fetchall()
            except Exception as e:
                raise Exception(e)
        
        return proxy_results

    def bulk_update_matchups(self, updates):
        """Update the matchups with new data values.

        updates : dict - Column names along with the new value.
        """
        try:
            self.__session.bulk_update_mappings(Matchups, updates)
            self.__session.commit()
        except Exception as e:
            raise Exception(e)
