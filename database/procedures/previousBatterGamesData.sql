delimiter //
CREATE PROCEDURE previousBatterGamesData (IN aGamePk INT, IN aBatterId INT, IN gamesBack INT)
BEGIN
	SELECT *
	FROM pitchData as p
	INNER JOIN(
		SELECT date,
			   m.gamePk as gamePk,
			   m.batterId as batterId
		FROM matchups as m
		INNER JOIN games as g ON m.gamePk = g.gamePk
		WHERE batterId = aBatterId AND date < (SELECT date FROM games WHERE gamePk = aGamePk)
		GROUP BY m.gamePk, date
		ORDER BY date DESC
		limit gamesBack
	) as previousGames USING (gamePk, batterId, date);
END//
delimiter ;
