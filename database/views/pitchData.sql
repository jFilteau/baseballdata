CREATE VIEW pitchData AS
SELECT date,
    officialName,
    pitcherName,
    batterName,
    pitcherId,
    batterId,
    pitchHand,
    batSide,
    batterPosition,
    m.atBatGameIndex,
    m.gamePk,
    f.matchupId,
    f.call,
    callCode,
    callCodeDescription,
    pitchType,
    eventName,
    inning,
    ballsCountBeforePitch,
    strikesCountBeforePitch,
    outsCountBeforePitch,
    zone,
    startSpeed,
    endSpeed,
    breakAngle,
    breakLength,
    breakY,
    spinRate,
    spinDirection,
    pitchNumber,
    p2.strikeZoneBottom,
    p2.strikeZoneTop,
    coordinates_pX,
    coordinates_pZ,
    f.playId,
    hitCoordinateX,
    hitCoordinateY,
    hitHardness,
    hitLaunchAngle,
    hitLaunchSpeed,
    hitLocation,
    hitTotalDistance,
    hitTrajectory
FROM pitchFx as f
INNER JOIN matchups as m ON f.matchupId = m.id
INNER JOIN games as g USING(gamePk)
INNER JOIN (
	SELECT id as officialHomeplate, fullName as officialName
    FROM officials
) as o USING(officialHomeplate)
INNER JOIN (
	SELECT distinct id as pitcherId,
        fullName as pitcherName,
        max(pitchHand) as pitchHand
    FROM players
	group by id, fullName
) as p1 USING(pitcherId)
INNER JOIN (
    SELECT distinct id as batterId,
	    fullName as batterName,
        max(strikeZoneTop) as strikeZoneTop,
        max(strikeZoneBottom) as strikeZoneBottom,
        max(batSide) as batSide,
        max(position) as batterPosition
    FROM players
	group by id, fullName
) as p2 USING(batterId);
