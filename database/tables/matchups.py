from sqlalchemy import Column, Integer, Boolean, ForeignKey, String, Float
from sqlalchemy.schema import UniqueConstraint
from database.tables.base import Base


class Matchups(Base):
    __tablename__  = "matchups"
    __table_args__ = (UniqueConstraint("gamePk", "atBatGameIndex", "batterId", "pitcherId", name="unique_at_bat"),)

    # Primary key
    id = Column(Integer, primary_key=True, nullable=False, autoincrement=True)

    # Foreign keys
    gamePk    = Column(Integer,    ForeignKey("games.gamePk"),         nullable=False, autoincrement=False)
    batterId  = Column(Integer,    ForeignKey("players.id"),           nullable=False, autoincrement=False)
    pitcherId = Column(Integer,    ForeignKey("players.id"),           nullable=False, autoincrement=False)
    runner1B  = Column(Integer,    ForeignKey("players.id"),           nullable=True,  autoincrement=False)
    runner2B  = Column(Integer,    ForeignKey("players.id"),           nullable=True,  autoincrement=False)
    runner3B  = Column(Integer,    ForeignKey("players.id"),           nullable=True,  autoincrement=False)
    eventName = Column(String(50), ForeignKey("playEvents.eventName"), nullable=False, autoincrement=False)

    # Non-nullables
    atBatGameIndex = Column(Integer, nullable=False, autoincrement=False)
    inning         = Column(Integer, nullable=False, autoincrement=False)
    strikes        = Column(Integer, nullable=False, autoincrement=False)
    balls          = Column(Integer, nullable=False, autoincrement=False)
    outs           = Column(Integer, nullable=False, autoincrement=False)

    # Nullables
    isScoringPlay       = Column(Boolean)
    atBatType           = Column(String(20), nullable=True, autoincrement=False)
    captivatingIndex    = Column(Integer,    nullable=True, autoincrement=False)
    homeScoreAfterEvent = Column(Integer,    nullable=True, autoincrement=False)
    awayScoreAfterEvent = Column(Integer,    nullable=True, autoincrement=False)
    runsBattedIn        = Column(Integer,    nullable=True, autoincrement=False)
    playId              = Column(String(50), nullable=True, autoincrement=False)
    hitLaunchSpeed      = Column(Float,      nullable=True, autoincrement=False)
    hitLaunchAngle      = Column(Float,      nullable=True, autoincrement=False)
    hitTotalDistance    = Column(Float,      nullable=True, autoincrement=False)
    hitTrajectory       = Column(String(30), nullable=True, autoincrement=False)
    hitHardness         = Column(String(30), nullable=True, autoincrement=False)
    hitLocation         = Column(Integer,    nullable=True, autoincrement=False)
    hitCoordinateX      = Column(Float,      nullable=True, autoincrement=False)
    hitCoordinateY      = Column(Float,      nullable=True, autoincrement=False)

    def __repr__(self):
        return "( gamePk = {pk}, atBatGameIndex = {index}, batter = {batter}, pitcher = {pitcher}, event = {event} )".format(
                pk=self.gamePk, index=self.atBatGameIndex, batter=self.batterId, pitcher=self.pitcherId, event=self.eventName)
