from sqlalchemy import Column, Integer, String
from database.tables.base import Base


class Officials(Base):
    __tablename__ = "officials"

    id       = Column(Integer,    primary_key=True, autoincrement=False)
    fullName = Column(String(50), autoincrement=False)

    def __repr__(self):
        return "( mlbId = {id}, fullName = {name} )".format(id=self.id, name=self.fullName)
