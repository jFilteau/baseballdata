import enum
from sqlalchemy import Column, Integer, String, Enum
from database.tables.base import Base


class LeagueEnum(enum.Enum):
    AL = 1
    NL = 2

class DivisionEnum(enum.Enum):
    East = 1
    Central = 2
    West = 3

class Teams(Base):
    __tablename__ = "teams"

    # Primary keys
    id = Column(Integer, primary_key=True, autoincrement=False)

    # Non-nullables
    fullName     = Column(String(30),         nullable=False, autoincrement=False)
    teamName     = Column(String(30),         nullable=False, autoincrement=False)
    locationName = Column(String(30),         nullable=False, autoincrement=False)
    abbreviation = Column(String(10),         nullable=False, autoincrement=False)
    league       = Column(Enum(LeagueEnum),   nullable=False, autoincrement=False)
    division     = Column(Enum(DivisionEnum), nullable=False, autoincrement=False)
    venueName    = Column(String(50),         nullable=False, autoincrement=False)

    def __repr__(self):
        return "( mlbId = {id}, fullName = {name}, league = {lg}, division = {div} )".format(
                id=self.id, name=self.fullName, lg=self.league, div=self.division)
