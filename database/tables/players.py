import enum
from sqlalchemy import Column, Integer, String, Enum, ForeignKey, Float
from database.tables.base import Base
from database.tables.teams import Teams


class PositionEnum(enum.Enum):
    # Infield
    Pitcher = "P"
    StartingPitcher = "SP"
    RightHandedPitcher = "RHP"
    LeftHandedPitcher = "LHP"
    RightHandedStarter = "RHS"
    LeftHandedStarter = "LHS"
    RightHandedReliever = "RHR"
    LeftHandedReliever = "LHR"
    ReliefPitcher = "RP"
    Closer = "CP"
    Catcher = "C"
    FirstBaseman = "1B"
    SecondBaseman = "2B"
    ThirdBaseman = "3B"
    Shortstop = "SS"
    Infielder = "IF"

    # Outfield
    LeftFielder = "LF"
    CenterFielder = "CF"
    RightFielder = "RF"
    Outfielder = "OF"

    # Other
    DesignatedHitter = "DH"
    PinchHitter = "PH"
    PinchRunner = "PR"
    BaseRunner = "BR"
    Utility = "UT"
    UtilityInfielder = "UI"
    UtilityOutfielder = "UO"
    Batter = "B"
    Unknown = "X"

class HandednessEnum(enum.Enum):
    Right = "R"
    Left = "L"
    Switch = "SwitchBatter"
    Either = "SwitchPitcher"

class Players(Base):
    __tablename__ = "players"

    # Primary keys
    id       = Column(Integer,            primary_key=True, nullable=False, autoincrement=False)
    position = Column(Enum(PositionEnum), primary_key=True, nullable=False, autoincrement=False)
    teamId   = Column(Integer, ForeignKey("teams.id"), primary_key=True,  nullable=False, autoincrement=False)

    # Non-nullables
    fullName         = Column(String(30),           nullable=False, autoincrement=False)
    firstName        = Column(String(30),           nullable=False, autoincrement=False)
    lastName         = Column(String(30),           nullable=False, autoincrement=False)
    strikeZoneTop    = Column(Float,                nullable=False, autoincrement=False)
    strikeZoneBottom = Column(Float,                nullable=False, autoincrement=False)
    batSide          = Column(Enum(HandednessEnum), nullable=False, autoincrement=False)
    pitchHand        = Column(Enum(HandednessEnum), nullable=False, autoincrement=False)

    # Nullables
    middleName = Column(String(30), nullable=True, autoincrement=False)

    def __repr__(self):
        return "( mlbId = {id}, fullName = {name}, team = {team}, position = {pos} )".format(
                id=self.id, name=self.fullName, team=self.teamId, pos=self.position)
