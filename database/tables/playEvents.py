from sqlalchemy import Column, Integer, String
from database.tables.base import Base


class PlayEvents(Base):
    __tablename__ = "playEvents"

    eventName = Column(String(50), primary_key=True, nullable=False, autoincrement=False)
    eventType = Column(String(50), nullable=False, autoincrement=False)

    def __repr__(self):
        return "( eventName = {name}, eventType={type} )".format(
                name=self.eventName, type=self.eventType)
