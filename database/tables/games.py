from sqlalchemy import Column, Integer, String, ForeignKey, Date
from database.tables.base import Base


class Games(Base):
    __tablename__ = "games"

    # Primary key
    gamePk = Column(Integer, primary_key=True, autoincrement=False, nullable=False)

    # Foreign keys
    awayTeam          = Column(Integer, ForeignKey("teams.id"),     nullable=False, autoincrement=False)
    homeTeam          = Column(Integer, ForeignKey("teams.id"),     nullable=False, autoincrement=False)
    officialHomeplate = Column(Integer, ForeignKey("officials.id"), nullable=True,  autoincrement =False)

    # Non-nullables
    date = Column(Date, nullable=False)

    # Nullables
    wind             = Column(String(30), nullable=True, autoincrement=False)
    temperature      = Column(String(30), nullable=True, autoincrement=False)
    weatherCondition = Column(String(30), nullable=True, autoincrement=False)
    dayOrNight       = Column(String(10), nullable=True, autoincrement=False)

    def __repr__(self):
        return "( gamePk = {pk}, date = {date}, homeTeam = {aTeam}, awayTeam = {hTeam} )".format(
                pk=self.gamePk, date=self.date, aTeam=self.awayTeam, hTeam=self.homeTeam)
