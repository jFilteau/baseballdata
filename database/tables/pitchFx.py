import enum
from sqlalchemy import Column, String, Integer, ForeignKey, Float, Enum
from sqlalchemy.schema import UniqueConstraint
from database.tables.base import Base


class CallCodeEnum(enum.Enum):
    Strike = "S"
    Ball = "B"
    InPlay = "X"

class PitchFx(Base):
    __tablename__  = "pitchFx"
    __table_args__ = (UniqueConstraint("matchupId", "pitchNumber", name="unique_pitch"),)

    # Primary key
    id = Column(Integer, primary_key=True, autoincrement=True, nullable=False)

    # Foreign keys
    matchupId = Column(Integer, ForeignKey("matchups.id"), nullable=False, autoincrement=False)

    # Non-nullables
    call                    = Column(Enum(CallCodeEnum), nullable=False, autoincrement=False)
    callCode                = Column(String(10),         nullable=False, autoincrement=False)
    callCodeDescription     = Column(String(30),         nullable=False, autoincrement=False)
    pitchType               = Column(String(10),         nullable=False, autoincrement=False)
    ballsCountBeforePitch   = Column(Integer,            nullable=False, autoincrement=False)
    strikesCountBeforePitch = Column(Integer,            nullable=False, autoincrement=False)
    outsCountBeforePitch    = Column(Integer,            nullable=False, autoincrement=False)

    # Nullables
    # For descriptions of these data points, see:
    # https://fastballs.wordpress.com/2007/08/02/glossary-of-the-gameday-pitch-fields/
    playId            = Column(String(50), nullable=True, autoincrement=False)
    zone              = Column(Integer,    nullable=True, autoincrement=False)
    startSpeed        = Column(Integer,    nullable=True, autoincrement=False)
    endSpeed          = Column(Integer,    nullable=True, autoincrement=False)
    breakAngle        = Column(Integer,    nullable=True, autoincrement=False)
    breakLength       = Column(Integer,    nullable=True, autoincrement=False)
    breakY            = Column(Integer,    nullable=True, autoincrement=False)
    spinRate          = Column(Integer,    nullable=True, autoincrement=False)
    spinDirection     = Column(Integer,    nullable=True, autoincrement=False)
    pitchNumber       = Column(Integer,    nullable=True, autoincrement=False)
    coordinates_aY    = Column(Float,      nullable=True, autoincrement=False)
    coordinates_aZ    = Column(Float,      nullable=True, autoincrement=False)  
    coordinates_pfxX  = Column(Float,      nullable=True, autoincrement=False)
    coordinates_pfxZ  = Column(Float,      nullable=True, autoincrement=False)
    coordinates_pX    = Column(Float,      nullable=True, autoincrement=False)
    coordinates_pZ    = Column(Float,      nullable=True, autoincrement=False)
    coordinates_vX0   = Column(Float,      nullable=True, autoincrement=False)
    coordinates_vY0   = Column(Float,      nullable=True, autoincrement=False)
    coordinates_vZ0   = Column(Float,      nullable=True, autoincrement=False)
    coordinates_x     = Column(Float,      nullable=True, autoincrement=False)
    coordinates_y     = Column(Float,      nullable=True, autoincrement=False)
    coordinates_x0    = Column(Float,      nullable=True, autoincrement=False)
    coordinates_y0    = Column(Float,      nullable=True, autoincrement=False)
    coordinates_z0    = Column(Float,      nullable=True, autoincrement=False)
    coordinates_aX    = Column(Float,      nullable=True, autoincrement=False)

    def __repr__(self):
        return "( matchupId = {matchupId}, pitchNumber = {pitchNum} )".format(
                matchupId=self.matchupId, pitchNum=self.pitchNumber)
