"""Replaced pitchFx gamePk and atBatGameIndex with matchupId foreign key

Revision ID: 254a438b233a
Revises: bf6ed9cc51fb
Create Date: 2019-09-21 17:01:05.571518

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = '254a438b233a'
down_revision = 'bf6ed9cc51fb'
branch_labels = None
depends_on = None


def upgrade():
    # Make sure to drop the foreign key constraint first.
    op.drop_constraint('pitchFx_ibfk_1', 'pitchFx', type_='foreignkey')
    op.drop_constraint('unique_pitch', 'pitchFx', type_='unique')

    op.drop_column('pitchFx', 'gamePk')
    op.drop_column('pitchFx', 'atBatGameIndex')
    op.add_column('pitchFx', sa.Column('matchupId', sa.Integer(), autoincrement=False, nullable=False))
    op.create_foreign_key(None, 'pitchFx', 'matchups', ['matchupId'], ['id'])
    op.create_unique_constraint('unique_pitch', 'pitchFx', ['matchupId', 'pitchNumber'])


def downgrade():
    # Make sure to drop the foreign key constraint first.
    op.drop_constraint(None, 'pitchFx', type_='foreignkey')
    op.drop_constraint('unique_pitch', 'pitchFx', type_='unique')

    op.drop_column('pitchFx', 'matchupId')
    op.add_column('pitchFx', sa.Column('atBatGameIndex', mysql.INTEGER(display_width=11), autoincrement=False, nullable=False))
    op.add_column('pitchFx', sa.Column('gamePk', mysql.INTEGER(display_width=11), autoincrement=False, nullable=False))
    op.create_foreign_key('pitchFx_ibfk_1', 'pitchFx', 'games', ['gamePk'], ['gamePk'])
    op.create_unique_constraint('unique_pitch', 'pitchFx', ['gamePk', 'atBatGameIndex', 'pitchNumber'])
