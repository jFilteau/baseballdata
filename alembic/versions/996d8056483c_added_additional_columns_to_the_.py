"""Added additional columns to the matchups table unique constraint

Revision ID: 996d8056483c
Revises: 254a438b233a
Create Date: 2019-10-18 01:34:41.988055

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '996d8056483c'
down_revision = '254a438b233a'
branch_labels = None
depends_on = None


def upgrade():
    # Make sure to drop the foreign key constraint first.
    op.drop_constraint('matchups_ibfk_3', 'matchups', type_='foreignkey')

    op.drop_constraint('unique_at_bat', 'matchups', type_='unique')
    op.create_unique_constraint('unique_at_bat', 'matchups', ['gamePk', 'atBatGameIndex', 'batterId', 'pitcherId'])

    op.create_foreign_key('matchups_ibfk_3', 'matchups', 'games', ['gamePk'], ['gamePk'])


def downgrade():
    # Make sure to drop the foreign key constraint first.
    op.drop_constraint('matchups_ibfk_3', 'matchups', type_='foreignkey')

    op.drop_constraint('unique_at_bat', 'matchups', type_='unique')
    op.create_unique_constraint('unique_at_bat', 'matchups', ['gamePk', 'atBatGameIndex'])

    op.create_foreign_key('matchups_ibfk_3', 'matchups', 'games', ['gamePk'], ['gamePk'])
